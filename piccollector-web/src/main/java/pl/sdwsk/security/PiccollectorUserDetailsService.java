package pl.sdwsk.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.sdwsk.model.User;
import pl.sdwsk.services.UserService;

/**
 * {@link UserDetailsService} that is wrapping user information received from {@link UserService} in
 * {@link PiccollectorUser} object.
 *
 * @author sdwsk
 */
@Service
public class PiccollectorUserDetailsService implements UserDetailsService
{

    private UserService userService;

    @Autowired
    public PiccollectorUserDetailsService(UserService userService)
    {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException
    {
        User user = userService.findByEmail(email);
        return new PiccollectorUser(user);
    }
}
