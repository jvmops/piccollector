package pl.sdwsk.security;

import org.springframework.security.core.authority.AuthorityUtils;
import pl.sdwsk.model.User;

/**
 * Implementation of {@link org.springframework.security.core.userdetails.UserDetails} that is serving as a
 * separation layer between Spring Security and Piccollector {@link User} model.
 *
 * @author sdwsk
 */
public class PiccollectorUser extends org.springframework.security.core.userdetails.User
{
    private Long id;
    private String username;
    private String email;

    public PiccollectorUser(User user)
    {
        super(user.getEmail(), user.getPasswordHash(), AuthorityUtils.createAuthorityList(user.getRole().toString()));
        this.id = user.getId();
        this.username = user.getUsername();
        this.email = user.getEmail();
    }

    @Override
    public String getUsername()
    {
        return username;
    }

    public String getEmail()
    {
        return email;
    }

    public Long getId()
    {
        return id;
    }
}
