package pl.sdwsk.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Bean that provides access to the piccollector properties defined in application.properties file. If specified
 * galleries root folder does not existsByEmail it attempts to create it at application startup.
 *
 * @author sdwsk
 */
@Component
@ConfigurationProperties(prefix = "piccollector")
public class PicProperties
{
    private static Logger log = LoggerFactory.getLogger(PicProperties.class);

    private String applicationUrl;
    private Path galleriesRoot;

    public String getApplicationUrl()
    {
        return applicationUrl;
    }

    public void setApplicationUrl(String applicationUrl)
    {
        this.applicationUrl = applicationUrl;
    }

    public Path getGalleriesRoot()
    {
        return galleriesRoot;
    }

    public void setGalleriesRoot(String galleriesRoot)
    {
        this.galleriesRoot = Paths.get(galleriesRoot);
    }
}
