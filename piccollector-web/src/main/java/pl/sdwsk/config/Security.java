package pl.sdwsk.config;

import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.rememberme.InMemoryTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

/**
 * Security config for piccollector application.
 *
 * @author sdwsk
 */
@Configuration
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
@EnableGlobalMethodSecurity(securedEnabled = true)
public class Security extends WebSecurityConfigurerAdapter
{

    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        http
            .authorizeRequests()
                .antMatchers("/", "/public/**", "/manager/**", "/user/register", "/error")
                    .permitAll()
                .anyRequest()
                    .fullyAuthenticated().and()
            .formLogin()
                .loginPage("/user/login")
                .failureUrl("/user/login?error")
                .defaultSuccessUrl("/galleries")
                .usernameParameter("email")
                .permitAll().and()
            .logout()
                .logoutUrl("/user/logout")
                .deleteCookies("remember-me")
                .logoutSuccessUrl("/?logout")
                .permitAll().and()
            .rememberMe()
                .tokenRepository(tokenRepository())
                .tokenValiditySeconds(60*60*24 * 15).and()
            .csrf().disable()
            .headers().frameOptions().disable();
    }

    private PersistentTokenRepository tokenRepository()
    {
        return new InMemoryTokenRepositoryImpl();
    }
}
