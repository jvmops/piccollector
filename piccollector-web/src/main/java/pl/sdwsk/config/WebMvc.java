package pl.sdwsk.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import pl.sdwsk.utils.GalleryResourceResolver;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author sdwsk
 */
@Configuration
public class WebMvc extends WebMvcConfigurerAdapter
{
    private static Logger logger = LoggerFactory.getLogger(WebMvc.class);

    @Autowired
    private PicProperties picProperties;

    @Value("${messages.spring.basename}")
    private String springMessages;

    @Value("${templates.message.location}")
    private String templatesPath;

    /**
     * This method is adding resource handlers for:
     *  - galleries content
     *  - public resources from client app
     * @param registry registry of resource handlers
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry)
    {
        registry.addResourceHandler("/galleries/**").addResourceLocations(picProperties.getGalleriesRoot().toUri().toString())
                .resourceChain(false) // TODO: lets have fun with caching
                .addResolver(new GalleryResourceResolver());
        registry.addResourceHandler("/public/**")
                .addResourceLocations("classpath:/public_res/");
    }

    /**
     * This bean intercepts dynamic locale changes via supplied request header. It serves
     * for debugging purposes.
     */
    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor()
    {
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName("Accept-Language");
        return lci;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry)
    {
        registry.addInterceptor(localeChangeInterceptor());
    }

    // TODO: In order to debug dynamic locale changes I setup LocaleChangeInterceptor instead of LocaleResolver.
    //    @Bean
    //    public LocaleResolver localeResolver()
    //    {
    //        SessionLocaleResolver slr = new SessionLocaleResolver();
    //        slr.setDefaultLocale(Locale.ENGLISH);
    //        return slr;
    //    }


    /**
     * This bean serves as a message localDate for views. It consists of paths to template's
     * messages sources resolved from application classpath and spring message localDate file name
     * that is serving messages for validation purposes.
     */
    @Bean
    public ReloadableResourceBundleMessageSource messageSource()
    {
        List<String> basenames = resolveMessageSourcesForTemplates();
        basenames.add(springMessages);

        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasenames(basenames.toArray(new String[basenames.size()]));
        messageSource.setCacheSeconds(60*60);
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

    @Override
    public Validator getValidator()
    {
        return validator();
    }

    /**
     * This method provides validator with configured message source.
     */
    @Bean
    public LocalValidatorFactoryBean validator()
    {
        LocalValidatorFactoryBean localValidatorFactory = new LocalValidatorFactoryBean();
        localValidatorFactory.setValidationMessageSource(messageSource());
        return localValidatorFactory;
    }

    /**
     * @return base names of message source files for templates
     */
    private List<String> resolveMessageSourcesForTemplates(){
        Resource[] resources = messageSources();
        return Arrays.stream(resources)
                .map(Resource::getFilename)
                .map(this::removeSuffix)
                .filter(name -> !name.endsWith("_pl"))
                .map(name -> this.templatesPath.concat(name))
                .collect(Collectors.toList());
    }

    /**
     * @return files containing messages for templates
     */
    private Resource[] messageSources() {
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        Path templatesDirectory = Paths.get(templatesPath);
        Path messageSources = templatesDirectory.resolve("*.properties");
        try
        {
            return resolver.getResources(messageSources.toString());
        }
        catch (IOException e)
        {
            logger.error("Unable to retrieve messages sources from {}", messageSources);
            throw new RuntimeException(e);
        }
    }

    /**
     * This method is removing suffix '.properties' from message source filename
     */
    private String removeSuffix(String filename) {
        int index = filename.indexOf(".");
        return filename.substring(0, index);
    }
}