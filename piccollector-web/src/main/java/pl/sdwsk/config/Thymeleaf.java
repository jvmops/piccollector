package pl.sdwsk.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.TemplateResolver;

/**
 * Piccollector's templating engine configuration.
 *
 * @author sdwsk
 */
@Configuration
public class Thymeleaf
{

    /**
     * This bean resolves template files from given classpath. Template files are served
     * from client application that is present in application context as .jar file.
     */
    @Bean
    public TemplateResolver templateResolver()
    {
        ClassLoaderTemplateResolver resolver = new ClassLoaderTemplateResolver();
        resolver.setPrefix("public_res/templates/");
        resolver.setSuffix(".html");
        resolver.setOrder(1);
        return resolver;
    }

    /**
     * Template engine setup. Spring Security Dialect enables using "sec:" tags
     * inside our templates that are resolving information from Spring Security.
     */
    @Bean
    public SpringTemplateEngine templateEngine()
    {
        SpringTemplateEngine engine = new SpringTemplateEngine();
        engine.setTemplateResolver(templateResolver());
        engine.addDialect(new SpringSecurityDialect());
        return engine;
    }

    /**
     * Character encoding configuration for ViewResolver.
     */
    @Bean
    public ViewResolver viewResolver()
    {
        ThymeleafViewResolver resolver = new ThymeleafViewResolver();
        resolver.setTemplateEngine(templateEngine());
        resolver.setCharacterEncoding("UTF-8");
        return resolver;
    }
}
