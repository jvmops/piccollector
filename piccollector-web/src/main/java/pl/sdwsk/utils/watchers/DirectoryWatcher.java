package pl.sdwsk.utils.watchers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.sdwsk.exceptions.DirectoryWatcherException;
import pl.sdwsk.repositories.GalleryRepository;
import pl.sdwsk.repositories.PictureRepository;
import pl.sdwsk.repositories.UserRepository;

import java.io.IOException;
import java.nio.file.*;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static java.nio.file.StandardWatchEventKinds.*;
import static pl.sdwsk.utils.Constants.DIRECTORY_ATTRIBUTES;

/**
 * This class is watching directories using {@link WatchService} API. After file insertion
 * into the registered directory ENTRY_CREATE event occurs. Following mechanism works similar
 * when we delete files from registered directories. Event handling is not implemented.
 * You should extend this class and override handleEventCreate() and handleEventDelete()
 * methods.
 *
 * @author sdwsk
 */
public abstract class DirectoryWatcher implements Runnable
{
    protected static Logger log = LoggerFactory.getLogger(DirectoryWatcher.class);
    protected static final String ANSI_GREEN = "\u001B[32m";
    protected static final String ANSI_RED = "\u001B[31m";
    protected static final String ANSI_RESET = "\u001B[0m";
    private static final WatchEvent.Kind<?>[] EVENTS = {ENTRY_CREATE, ENTRY_DELETE};

    protected Path galleriesRoot;
    protected GalleryRepository galleryRepository;
    protected PictureRepository pictureRepository;
    protected UserRepository userRepository;
    private WatchService watchService;
    private Map<WatchKey,Path> keys;

    public DirectoryWatcher(GalleryRepository galleryRepository) {
        this.galleryRepository = galleryRepository;
        this.watchService = getWatchService();
        this.keys = new ConcurrentHashMap<>();
    }

    public DirectoryWatcher(
            GalleryRepository galleryRepository,
            PictureRepository pictureRepository,
            UserRepository userRepository,
            Path galleriesRoot) {
        this.galleriesRoot = galleriesRoot;
        this.galleryRepository = galleryRepository;
        this.pictureRepository = pictureRepository;
        this.userRepository = userRepository;
        this.watchService = getWatchService();
        this.keys = new ConcurrentHashMap<>();
    }

    private WatchService getWatchService() {
        try {
            return FileSystems.getDefault().newWatchService();
        }
        catch (IOException e) {
            throw new DirectoryWatcherException("Unable to create WatchService.", e);
        }
    }

    @Override
    public void run()
    {
        processEvents();
    }

    /**
     * Each registered directory is watched for events that signals file creation/deletion.
     *
     * @param directory path to the directory
     */
    public void register(Path directory) {
        try {
            Files.createDirectories(directory, DIRECTORY_ATTRIBUTES);
            WatchKey watchKey = directory.register(this.watchService, EVENTS);
            keys.put(watchKey, directory);
        }
        catch (IOException e) {
            log.error("Unable to register directory watcher for {}", directory);
        }
    }

    public void close() throws IOException {
        watchService.close();
    }

    /**
     * This method enters the infinity loop where it waits for events to appear.
     */
    private void processEvents() {
        for (;;) {
            WatchKey key;

            // wait for key to be signalled
            try {
                key = watchService.take();
            } catch (InterruptedException e) {
                log.error("Error during waiting for directory event.");
                break;
            } catch (ClosedWatchServiceException e) {
                log.warn("WatchService has been closed while thread was waiting for an event. {}");
                break;
            }

            Path dir = keys.get(key);
            if (dir == null) {
                key.pollEvents().stream()
                        .map(this::cast)
                        .map(WatchEvent::context)
                        .forEach((ctx) -> log.error("WatchKey not recognized for context: {}", ctx.toString()));
                continue;
            }

            for (WatchEvent<?> event: key.pollEvents()) {
                WatchEvent.Kind kind = event.kind();
                if (kind == OVERFLOW) {
                    log.error("DirectoryWatcher OVERFLOW event occured for {}. Check data integrity.", dir.toString());
                    continue;
                }

                // Context for directory entry event is the file name of entry
                WatchEvent<Path> ev = cast(event);
                Path contextResourceName = ev.context();

                if(event.kind() == ENTRY_CREATE)
                    handleEventCreate(dir.resolve(contextResourceName));
                else if (event.kind() == ENTRY_DELETE)
                    handleEventDelete(dir.resolve(contextResourceName));
            }

            // reset key and remove from set if directory no longer accessible
            boolean valid = key.reset();
            if (!valid) {
                keys.remove(key);
                // all directories are inaccessible
                if (keys.isEmpty())
                    break;
            }
        }
    }

    @SuppressWarnings("unchecked")
    private <T> WatchEvent<T> cast(WatchEvent<?> event)
    {
        return (WatchEvent<T>)event;
    }

    /**
     * You should override this method in order to handle create event
     *
     * @param contextPath relative path to the context of an event
     */
    protected abstract void handleEventCreate(Path contextPath);

    /**
     * You should override this method in order to handle delete event
     *
     * @param contextPath relative path to the context of an event
     */
    protected abstract void handleEventDelete(Path contextPath);
}
