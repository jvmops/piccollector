package pl.sdwsk.utils.watchers;

import pl.sdwsk.model.Gallery;
import pl.sdwsk.repositories.GalleryRepository;
import pl.sdwsk.repositories.PictureRepository;
import pl.sdwsk.repositories.UserRepository;
import pl.sdwsk.utils.GalleryUtils;

import java.nio.file.Path;

/**
 * This class is watching galleries root directory for changes. When new
 * gallery pops up it's meta-data is persisted into database. Gallery's
 * directory is then registered with {@link PictureWatcher} which listens
 * for incoming pictures.
 *
 * @author sdwsk
 */
public class GalleryWatcher extends DirectoryWatcher
{
    private DirectoryWatcher pictureWatcher;

    public GalleryWatcher(GalleryRepository galleryRepository, DirectoryWatcher pictureWatcher)
    {
        super(galleryRepository);
        this.pictureWatcher = pictureWatcher;
    }

    @Override
    protected void handleEventCreate(Path contextPath)
    {
        Gallery gallery = GalleryUtils.createGalleryFrom(contextPath.getFileName());
        galleryRepository.save(gallery);
        pictureWatcher.register(contextPath);
        log.info("Gallery's meta-data persisted into database. Gallery added to picture watcher: "
                + ANSI_RED + "{}" + ANSI_RESET, gallery.getPath().toString());
    }

    protected void handleEventDelete(Path contextPath)
    {
        throw new UnsupportedOperationException();
    }
}
