package pl.sdwsk.utils.watchers;

import pl.sdwsk.model.Picture;
import pl.sdwsk.repositories.GalleryRepository;
import pl.sdwsk.repositories.PictureRepository;
import pl.sdwsk.repositories.UserRepository;
import pl.sdwsk.utils.GalleryUtils;

import java.nio.file.Path;

/**
 * This watcher is watching gallery's directories. If new picture
 * is added to the directory, picture's meta-data is persisted
 * into database.
 *
 * @author sdwsk
 */
public class PictureWatcher extends DirectoryWatcher
{
    public PictureWatcher(
            GalleryRepository galleryRepository,
            PictureRepository pictureRepository,
            UserRepository userRepository,
            Path galleriesRoot)
    {
        super(galleryRepository, pictureRepository, userRepository, galleriesRoot);
    }

    @Override
    protected void handleEventCreate(Path picturePath)
    {
        Picture picture = GalleryUtils.createPicture(picturePath, galleriesRoot, galleryRepository, userRepository);
        pictureRepository.save(picture);
        log.info("Picture's meta-data persisted into database: "
                + ANSI_GREEN + "{}" + ANSI_RESET, galleriesRoot.relativize(picturePath).toString());
    }

    protected void handleEventDelete(Path picturePath)
    {
        throw new UnsupportedOperationException();
    }
}
