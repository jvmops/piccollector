package pl.sdwsk.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import pl.sdwsk.config.PicProperties;
import pl.sdwsk.controllers.response_objects.PictureInfo;
import pl.sdwsk.exceptions.FileUploadException;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static pl.sdwsk.utils.GalleryResource.*;

/**
 * This class is encapsulating operations on MultipartFiles.
 *
 * @author sdwsk
 */
@Component
public class UploadedFileProcessor
{
    private static Logger log = LoggerFactory.getLogger(UploadedFileProcessor.class);

    private final Path galleriesRoot;
    private final String applicationUrl;

    @Autowired
    public UploadedFileProcessor(PicProperties picProperties)
    {
        this.galleriesRoot = picProperties.getGalleriesRoot();
        this.applicationUrl = picProperties.getApplicationUrl();
    }

    /**
     * Resolves new filename for uploaded picture. Pattern for filename is
     * {author}_{timestamp}.{extension}
     *
     * @param author name of the author
     * @param timestamp creation time of picture
     * @param extension picture format
     * @return filename
     */
    public Path resolveNewFilename(String author, long timestamp, String extension)
    {
        String newFilename = author + CONTENT_SEPARATOR + timestamp + extension;
        return Paths.get(newFilename);
    }

    /**
     * Transfers uploaded picture into a destination directory
     *
     * @param uploadedPicture uploaded file
     * @param destination gallery folder name + file name
     * @return relative url to the saved picture
     */
    public String transferPicture(
            MultipartFile uploadedPicture,
            Path destination)
    {
        Path absoluteDestination = this.galleriesRoot.resolve(destination);
        transfer(uploadedPicture, absoluteDestination);
        return getUrl(destination);
    }

    private String getUrl(Path location)
    {
        String relativeUrl = new GalleryResource(location).getResourceUrl();
        return this.applicationUrl + relativeUrl;
    }

    private void transfer(MultipartFile picture, Path destination)
    {
        try
        {
            picture.transferTo(destination.toFile());
            log.info("Picture {} saved into {}", picture.getOriginalFilename(), destination.toString());
        }
        catch (IOException e)
        {
            throw new FileUploadException(picture.getOriginalFilename(), destination, e);
        }
    }
}
