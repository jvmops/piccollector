package pl.sdwsk.utils;

/**
 * @author sdwsk
 */
public final class NullUtils
{
    /**
     * Returns default Integer value if passed Integer is null. Otherwise returns passed Integer.
     */
    public static Integer defaultValueIfNull(Integer value, Integer defaultValue)
    {
        return value == null ? defaultValue : value;
    }
}
