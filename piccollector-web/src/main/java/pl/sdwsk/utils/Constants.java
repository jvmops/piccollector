package pl.sdwsk.utils;

import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Set;

/**
 * @author sebastian.sadowski@gmail.com
 */
public class Constants {
    public static final FileAttribute<Set<PosixFilePermission>> DIRECTORY_ATTRIBUTES;

    static {
        Set<PosixFilePermission> perms = PosixFilePermissions.fromString("rw-r--r--");
        DIRECTORY_ATTRIBUTES = PosixFilePermissions.asFileAttribute(perms);
    }
}
