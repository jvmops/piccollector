package pl.sdwsk.utils;

import org.springframework.core.io.Resource;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.resource.AbstractResourceResolver;
import org.springframework.web.servlet.resource.ResourceResolverChain;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

/**
 * {@code ResourceResolver} that is responsible for supporting nicely formatted picture's url.
 * Given relative url:
 *      {date}/{event-name}/{author}_{pictureNumber}.{jpg|png}
 * it is resolving a resource from relative path:
 *      {date_event-name}/{author}_{pictureNumber}.{jpg|png}
 *
 * @author sdwsk
 */
public class GalleryResourceResolver extends AbstractResourceResolver {

    @Override
    protected Resource resolveResourceInternal(HttpServletRequest httpServletRequest, String requestPath, List<? extends Resource> locations, ResourceResolverChain resourceResolverChain)
    {
        GalleryResource galleryResource = new GalleryResource(requestPath);
        String resourcePath = galleryResource.getResourcePath();
        return getResource(resourcePath, locations);
    }

    @Override
    protected String resolveUrlPathInternal(String resourcePath, List<? extends Resource> locations, ResourceResolverChain resourceResolverChain)
    {
        GalleryResource galleryResource = new GalleryResource(resourcePath);
        String resourceUrl = galleryResource.getResourceUrl();
        return (StringUtils.hasText(resourceUrl) && getResource(resourcePath, locations) != null ? resourceUrl : null);
    }

    private Resource getResource(String resourcePath, List<? extends Resource> locations)
    {
        for (Resource location : locations)
        {
            try
            {
                if (logger.isTraceEnabled())
                    logger.trace("Checking location: " + location);
                Resource resource = getResource(resourcePath, location);
                if (resource != null)
                {
                    if (logger.isTraceEnabled())
                        logger.trace("Found match: " + resource);
                    return resource;
                }
                else if (logger.isTraceEnabled())
                    logger.trace("No match for location: " + location);
            }
            catch (IOException ex)
            {
                logger.trace("Failure checking for relative resource - trying next location", ex);
            }
        }
        return null;
    }

    private Resource getResource(String resourcePath, Resource location) throws IOException
    {
        Resource resource = location.createRelative(resourcePath);
        if (resource.exists() && resource.isReadable())
            return resource;
        return null;
    }
}
