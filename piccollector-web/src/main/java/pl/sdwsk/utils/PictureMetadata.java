package pl.sdwsk.utils;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import org.springframework.web.multipart.MultipartFile;
import pl.sdwsk.exceptions.FileUploadException;

import java.io.IOException;
import java.util.Date;

/**
 * @author sdwsk
 */
public class PictureMetadata
{
    public static long getCreationTime(MultipartFile uploadedFile)
    {
        Metadata metadata = readMetadata(uploadedFile);
        if(metadata.containsDirectoryOfType(ExifSubIFDDirectory.class)) {
            ExifSubIFDDirectory directory = metadata.getFirstDirectoryOfType(ExifSubIFDDirectory.class);
            if (directory.containsTag(ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL)) {
                Date date = directory.getDate(ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL);
                return date.getTime() / 1000;
            }
        }
        return System.currentTimeMillis();
    }

    private static Metadata readMetadata(MultipartFile multipartFile)
    {
        try
        {
            return ImageMetadataReader.readMetadata(multipartFile.getInputStream());
        }
        catch (ImageProcessingException e)
        {
            throw new FileUploadException(e.getMessage(), e);
        }
        catch (IOException e)
        {
            throw new FileUploadException(e.getMessage(), e);
        }
    }
}
