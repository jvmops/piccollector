package pl.sdwsk.utils;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Metadata;
import org.springframework.web.multipart.MultipartFile;
import pl.sdwsk.exceptions.FileUploadException;
import pl.sdwsk.exceptions.GalleryNotFoundException;
import pl.sdwsk.exceptions.UserNotFoundException;
import pl.sdwsk.model.Gallery;
import pl.sdwsk.model.Picture;
import pl.sdwsk.repositories.GalleryRepository;
import pl.sdwsk.repositories.UserRepository;

import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.*;

/**
 * @author sdwsk
 */
public class GalleryUtils
{

    /**
     * Persist demo gallery into database
     *
     * @param demoGallery path to the gallery
     */
    public static void saveGallery(Path demoGallery, GalleryRepository galleryRepository)
    {
        Path galleryRelativePath = demoGallery.getFileName();
        GalleryResource galleryResource = new GalleryResource(galleryRelativePath);
        String eventName = galleryResource.getEventName();
        LocalDate eventDate = LocalDate.parse(galleryResource.getEventDate());
        String url = galleryResource.getResourceUrl();
        Gallery gallery = new Gallery(eventName, eventDate, galleryRelativePath, url);
        galleryRepository.save(gallery);
    }

    /**
     * Creates picture to be persisted to database from absolute path to the picture
     *
     * @param picturePath absolute path to the picture
     * @param galleriesRoot root of galleries
     * @return picture to be persisted
     */
    public static Picture createPicture(
            Path picturePath,
            Path galleriesRoot,
            GalleryRepository galleryRepository,
            UserRepository userRepository)
    {
        Path relativePicturePath = galleriesRoot.relativize(picturePath);
        GalleryResource galleryResource = new GalleryResource(relativePicturePath);
        long galleryId = getGalleryId(relativePicturePath, galleryRepository);
        long size = FileUtils.getSize(picturePath);
        String url = galleryResource.getResourceUrl();
        return new Picture(
                galleryRepository.findById(galleryId)
                        .orElseThrow(() -> new GalleryNotFoundException(galleryId)),
                userRepository.findByUsername(galleryResource.getAuthor())
                        .orElseThrow(() -> new UserNotFoundException(galleryResource.getAuthor())),
                size,
                url,
                picturePath.getFileName());
    }

    private static long getGalleryId(Path relativePicturePath, GalleryRepository galleryRepository)
    {
        Path galleryPath = relativePicturePath.getParent();
        return galleryRepository.findIdByPath(galleryPath)
                .orElseThrow(() -> new GalleryNotFoundException(galleryPath));
    }

    /**
     * This utility map pictures to the author.
     *
     * @param allPictures collection of pictures
     * @return pictures mapped to the author's name
     */
    public static Map<String, Collection<Picture>> mapPicturesToAuthor(
            Collection<Picture> allPictures,
            String currentUsername)
    {
        // TODO: check map.computeIfAbsent/present and stream grouping functions
        Map<String, Collection<Picture>> picturesByAuthor = new LinkedHashMap<>();

        // it appears first
        picturesByAuthor.put(currentUsername, new ArrayList<>());

        for (Picture picture : allPictures)
        {
            String author = picture.getAuthor().getUsername();
            if (!picturesByAuthor.containsKey(author))
                picturesByAuthor.put(author, new ArrayList<>());
            Collection<Picture> pictures = picturesByAuthor.get(author);
            pictures.add(picture);
        }
        // if there is no pictures from principal in gallery remove it from map
        if (picturesByAuthor.get(currentUsername).isEmpty())
            picturesByAuthor.remove(currentUsername);

        return picturesByAuthor;
    }

    public static Gallery createGalleryFrom(Path directoryName) {
        GalleryResource galleryResource = new GalleryResource(directoryName);
        return new Gallery(
                galleryResource.getEventName(),
                LocalDate.parse(galleryResource.getEventDate()),
                directoryName,
                galleryResource.getResourceUrl()
        );
    }
}
