package pl.sdwsk.utils;

import pl.sdwsk.exceptions.GalleryResourceException;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Arrays;

/**
 * Parser that reads gallery's or picture's meta-data from provided url/path.
 *
 * Directory name convention: {date}_{event-name}
 * Picture (filename) convention: {author}_{pictureNumber}.{jpg|png}
 * Sample picture url: {date}/{event-name}/{author}_{pictureNumber}.{jpg|png}
 * Date format: yyyy-MM-dd
 *
 * @author sdwsk
 */
public class GalleryResource {
    public static final String CONTENT_SEPARATOR = "_";
    private static final String TEXT_SEPARATOR = "-";
    private static final String SEPARATOR = FileSystems.getDefault().getSeparator();
    private static final String REQUEST_PATH_SEPARATOR = "/";
    private static final String EXTENSION_SEPARATOR = "\\.";
    private static final String OR = "|";
    private static final int DATE = 0;
    private static final int EVENT_NAME = 1;
    private static final int AUTHOR = 2;
    private static final int TIMESTAMP = 3;
    private static final int FORMAT = 4;
    private static final String DATE_FORMAT = "([0-9]{4})-([0-9]{2})-([0-9]{2})";

    private final String[] resourceData;
    private final boolean isPicture;

    public GalleryResource(String url)
    {
        this.resourceData = url.split(REQUEST_PATH_SEPARATOR + OR + CONTENT_SEPARATOR + OR + EXTENSION_SEPARATOR);
        checkDataIntegrity();
        this.isPicture = isPicture();
    }

    public GalleryResource(Path resourcePath)
    {
        this.resourceData = resourcePath.toString().split(SEPARATOR + OR + CONTENT_SEPARATOR + OR + EXTENSION_SEPARATOR);
        checkDataIntegrity();
        this.isPicture = isPicture();
    }

    public String getEventDate()
    {
        return resourceData[DATE];
    }

    /**
     * Method removes dashes '-' from event name.
     *
     * @return event name
     */
    public String getEventName()
    {
        return resourceData[EVENT_NAME].replaceAll(TEXT_SEPARATOR, " ");
    }

    public String getAuthor()
    {
        if (!this.isPicture)
            throw new GalleryResourceException(String.format("This resource is not a picture: %s", getResourcePath()));
        return resourceData[AUTHOR];
    }

    /**
     * @return ordinal number of picture uploaded by this author
     */
    public long getCreationTime()
    {
        if (!this.isPicture)
            throw new GalleryResourceException(String.format("This resource is not a picture: %s", getResourcePath()));
        return Long.parseLong(resourceData[TIMESTAMP]);
    }

    /**
     * This method converts resource path into an prettified url
     *
     * @return relative url to the resource
     */
    public String getResourceUrl()
    {
        String resourceRelativeUrl = getGalleryUrl();
        if(this.isPicture())
            resourceRelativeUrl = getPictureUrl(resourceRelativeUrl);
        return resourceRelativeUrl;
    }

    /**
     * Pretty urls need to be converted before resource can be fetched. This
     * method serves as a converter requested URL -> path.
     *
     * @return relative resource path as String
     */
    public String getResourcePath()
    {
        String resourcePath = getDirPath();
        if (this.isPicture)
            resourcePath = getFilePath(resourcePath);
        return resourcePath;
    }

    private void checkDataIntegrity()
    {
        if (!resourceData[0].matches(DATE_FORMAT))
            throw new GalleryResourceException(String.format("Incorrect input. Supplied data: %s", Arrays.toString(resourceData)));
    }

    private boolean isPicture()
    {
        return this.resourceData.length > 2;
    }

    private String getDirPath()
    {
        return resourceData[DATE] + CONTENT_SEPARATOR + resourceData[EVENT_NAME];
    }

    private String getFilePath(String dirPath)
    {
        return dirPath + "/" + resourceData[AUTHOR] + CONTENT_SEPARATOR + resourceData[TIMESTAMP] + "." + resourceData[FORMAT];
    }

    private String getGalleryUrl()
    {
        return "/" + resourceData[DATE] + "/" + resourceData[EVENT_NAME];
    }

    private String getPictureUrl(String galleryUrl)
    {
        return galleryUrl + "/" + resourceData[AUTHOR] + CONTENT_SEPARATOR + resourceData[TIMESTAMP]
                + "." + resourceData[FORMAT];
    }
}
