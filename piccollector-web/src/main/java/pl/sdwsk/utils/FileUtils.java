package pl.sdwsk.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * This helper interface provides utilities for working with Files.
 *
 * @author sdwsk
 */
public final class FileUtils
{
    private static Logger logger = LoggerFactory.getLogger(FileUtils.class);

    /**
     * This utility recursively removes directory.
     *
     * @param dirPath directory to be removed
     * @throws IOException
     */
    public static void delete(Path dirPath) throws IOException
    {
        Files.walkFileTree(dirPath, new SimpleFileVisitor<Path>()
        {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException
            {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException
            {
                Files.delete(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    /**
     * @param file uploaded file
     * @return String representation of uploaded file's extension
     */
    public static String getFileExtension(MultipartFile file)
    {
        String name = file.getOriginalFilename();
        return name.substring(name.lastIndexOf("."));
    }

    /**
     * @param filePath absolute file Path
     * @return size of the file
     */
    static long getSize(Path filePath)
    {
        BasicFileAttributeView fileAttrsView = Files.getFileAttributeView(filePath, BasicFileAttributeView.class);
        BasicFileAttributes fileAttrs;
        try
        {
            fileAttrs = fileAttrsView.readAttributes();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
        return fileAttrs.size();
    }

    public static void createDirectory(Path directoryPath)
    {
        try
        {
            Files.createDirectories(directoryPath);
            logger.info("Directory created: {}", directoryPath.toAbsolutePath().toString());
        }
        catch (IOException e)
        {
            logger.info("Unable to create directory: {}", directoryPath.toAbsolutePath().toString(), e);
        }
    }
}
