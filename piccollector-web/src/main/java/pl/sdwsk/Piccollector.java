package pl.sdwsk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

/**
 * Entry point for application. Remember, that you need to provide a property
 * with a name of "external.properties" from servlet context. This property
 * MUST contain a valid path to a file that MUST contain following properties:

     # main config
     spring.application.name=piccollector
     piccollector.application-url=http://localhost:8080/galleries/
     piccollector.galleries-root=/home/user/Pictures/galleries

     # datasource config
     spring.datasource.currentUsername=
     spring.datasource.password=
     spring.datasource.url=
     spring.datasource.driver-class-name=
     spring.jpa.database-platform=org.hibernate.dialect.PostgreSQLDialect

 *
 * @author sdwsk
 */
@SpringBootApplication
@PropertySources({@PropertySource(value={"classpath:application.properties"}),
		          @PropertySource(value={"file:${external.properties}"}, ignoreResourceNotFound = true)})
public class Piccollector
{
	public static void main(String[] args)
	{
		SpringApplication.run(Piccollector.class, args);
	}
}
