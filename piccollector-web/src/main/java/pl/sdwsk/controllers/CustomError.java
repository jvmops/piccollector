package pl.sdwsk.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * Error controller for custom error page.
 *
 * @author sdwsk
 */
@Controller
public class CustomError implements ErrorController
{
    private static final String PATH = "/error";

    private ErrorAttributes errorAttributes;

    @Autowired
    public CustomError(ErrorAttributes errorAttributes)
    {
        this.errorAttributes = errorAttributes;
    }

    @RequestMapping(value = PATH, method = RequestMethod.GET)
    String error(
            HttpServletRequest request,
            Model model)
    {
        Map<String, Object> attrs = getErrorAttributes(request, false);
        model.addAttribute("errorAttributes", attrs);
        return "error";
    }

    @RequestMapping(value = PATH, method = RequestMethod.POST)
    @ResponseBody
    Map<String, Object> error(HttpServletRequest request)
    {
        Map<String, Object> attrs = getErrorAttributes(request, false);
        return attrs;
    }

    @Override
    public String getErrorPath()
    {
        return PATH;
    }

    private Map<String, Object> getErrorAttributes(
            HttpServletRequest request,
            boolean includeStackTrace)
    {
        RequestAttributes requestAttributes = new ServletRequestAttributes(request);
        return errorAttributes.getErrorAttributes(requestAttributes, includeStackTrace);
    }
}
