package pl.sdwsk.controllers.form_objects;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import pl.sdwsk.model.User;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author sdwsk
 */
public class RegistrationDetails
{
    private String username = "";
    private String email = "";
    private String password = "";
    private String passwordConfirmation = "";
    private User.Role role = User.Role.ROLE_USER;

    @NotEmpty(message = "{username.empty}")
    @Size(min=3, message = "{username.too_short}")
    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    @NotEmpty(message = "{email.empty}")
    @Email(message = "{email.bad_format}")
    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    @NotEmpty(message = "{password.empty}")
    @Size(min = 6, message = "{password.too_short}")
    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    @NotNull
    public String getPasswordConfirmation()
    {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation)
    {
        this.passwordConfirmation = passwordConfirmation;
    }

    public User.Role getRole()
    {
        return role;
    }

    public void setRole(User.Role role)
    {
        this.role = role;
    }
}
