package pl.sdwsk.controllers.form_objects;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author sdwsk
 */
public class UploadedPictures
{
    private String author;
    private List<MultipartFile> files;
    private int galleryId;

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public List<MultipartFile> getFiles()
    {
        return files;
    }

    public void setFiles(List<MultipartFile> files)
    {
        this.files = files;
    }

    public void setGalleryId(int galleryId)
    {
        this.galleryId = galleryId;
    }

    public int getGalleryId()
    {
        return galleryId;
    }
}
