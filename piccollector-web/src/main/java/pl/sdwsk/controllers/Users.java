package pl.sdwsk.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.sdwsk.config.PicProperties;
import pl.sdwsk.controllers.form_objects.ChangedPassword;
import pl.sdwsk.controllers.form_objects.RegistrationDetails;
import pl.sdwsk.services.UserService;
import pl.sdwsk.validation.ChangedPasswordValidator;
import pl.sdwsk.validation.RegistrationDetailsValidator;

import javax.validation.Valid;
import java.security.Principal;

/**
 * @author sdwsk
 */
@Controller
public class Users {
    private UserService userService;
    private RegistrationDetailsValidator registrationDetailsValidator;
    private ChangedPasswordValidator changedPasswordValidator;
    private String url;

    @Autowired
    public Users(
            UserService userService,
            RegistrationDetailsValidator registrationDetailsValidator,
            ChangedPasswordValidator changedPasswordValidator,
            PicProperties picProperties) {
        this.userService = userService;
        this.registrationDetailsValidator = registrationDetailsValidator;
        this.changedPasswordValidator = changedPasswordValidator;
        this.url = picProperties.getApplicationUrl();
    }

    @InitBinder("registrationDetails")
    public void bindRegistrationDetails(WebDataBinder binder)
    {
        binder.addValidators(registrationDetailsValidator);
    }

    @InitBinder("changedPassword")
    public void addChangedPasswordValidator(WebDataBinder binder)
    {
        binder.addValidators(changedPasswordValidator);
    }

    @RequestMapping(
            value = "/user/login",
            method = RequestMethod.GET)
    public String login()
    {
        return "login";
    }

    @RequestMapping(
            value = "/user/register",
            method = RequestMethod.GET)
    public String register(Model model)
    {
        model.addAttribute("registrationDetails", new RegistrationDetails());
        return "register";
    }

    @RequestMapping(
            value = "/user/register",
            method = RequestMethod.POST)
    public String createUser(
            @Valid @ModelAttribute("registrationDetails") RegistrationDetails registrationDetails,
            BindingResult bindingResult)
    {
        if (bindingResult.hasErrors())
            return "register";
        userService.register(registrationDetails);
        return "redirect:/user/login";
    }

    @RequestMapping(
            value = "/user/changepass",
            method = RequestMethod.GET)
    public String changePassword(
            Model model,
            Principal principal) {
        ChangedPassword changedPassword = new ChangedPassword();
        changedPassword.setUserName(principal.getName());
        model.addAttribute("changedPassword", changedPassword);
        return "changepass";
    }

    @RequestMapping(
            value = "/user/changepass",
            method = RequestMethod.POST)
    public String changePassword(
            @Valid @ModelAttribute("changedPassword") ChangedPassword changedPassword,
            BindingResult bindingResult,
            RedirectAttributes redirectAttributes,
            Principal principal)
    {
        if (bindingResult.hasErrors())
            return "changepass";
        userService.changePass(changedPassword);
        redirectAttributes.addAttribute("passChanged", true);
        return "redirect:" + url;
    }
}
