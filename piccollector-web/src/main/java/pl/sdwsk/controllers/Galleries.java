package pl.sdwsk.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.sdwsk.config.PicProperties;
import pl.sdwsk.controllers.form_objects.UploadedPictures;
import pl.sdwsk.controllers.response_objects.GalleryContent;
import pl.sdwsk.controllers.response_objects.PictureInfo;
import pl.sdwsk.model.Gallery;
import pl.sdwsk.model.Picture;
import pl.sdwsk.security.PiccollectorUser;
import pl.sdwsk.services.GalleryService;

import javax.servlet.http.HttpServletRequest;
import java.nio.file.Paths;
import java.security.Principal;
import java.time.LocalDate;
import java.util.List;

import static pl.sdwsk.utils.NullUtils.defaultValueIfNull;

/**
 * @author sdwsk
 */
@Controller
public class Galleries
{
    private GalleryService galleryService;
    private String applicationUrl;

    @Autowired
    public Galleries(GalleryService galleryService, PicProperties picProperties)
    {
        this.galleryService = galleryService;
        this.applicationUrl = picProperties.getApplicationUrl();
    }

    @RequestMapping(
            value = "/galleries",
            method = RequestMethod.GET)
    public String galleries(@RequestParam(required = false) Integer page, Model model)
    {
        page = defaultValueIfNull(page, 0);
        Page<Gallery> galleries =  galleryService.getGalleries(page);
        model.addAttribute("galleries", galleries);
        model.addAttribute("applicationUrl", applicationUrl);
        return "galleries";
    }

    @RequestMapping(
            value = "/galleries",
            method = RequestMethod.POST)
    public String createGallery(
            @RequestParam(name = "galleryName") String galleryName,
            @RequestParam(name = "eventDate") String eventDate)
    {
        galleryService.createGallery(galleryName, LocalDate.parse(eventDate));
        return "redirect:/galleries";
    }

    @RequestMapping(
            value = "/galleries/{eventDate}/{eventName}",
            method = RequestMethod.GET)
    public String showGallery(
            @PathVariable String eventDate,
            @PathVariable String eventName,
            Principal principal,
            Model model)
    {
        Gallery gallery = galleryService.findGalleryByEventNameAndDate(eventName, LocalDate.parse(eventDate));
        GalleryContent galleryContent = galleryService.galleryContent(gallery, principal.getName());
        model.addAttribute("applicationUrl", applicationUrl);
        model.addAttribute("galleryContent", galleryContent);
        model.addAttribute("uploadedPictures", new UploadedPictures());
        return "gallery";
    }

    @ResponseBody
    @RequestMapping(
            value = "/galleries/{eventDate}/{eventName}",
            method = RequestMethod.POST)
    public PictureInfo uploadPicture(
            @PathVariable String eventDate,
            @PathVariable String eventName,
            @RequestParam("uploadedFile") MultipartFile uploadedFile,
            Principal principal)
    {
        Gallery gallery = galleryService.findGalleryByEventNameAndDate(eventName, LocalDate.parse(eventDate));
        return galleryService.addPicture(uploadedFile, gallery, principal.getName());
    }

    @ResponseBody
    @RequestMapping(
            value = "/galleries/{eventDate}/{galleryName}",
            method = RequestMethod.POST,
            params = "fileName",
            produces = "application/json")
    public PictureInfo setCover(@RequestParam("fileName") String fileName)
    {
        PictureInfo pictureInfo = galleryService.setCover(Paths.get(fileName));
        return pictureInfo;
    }
}
