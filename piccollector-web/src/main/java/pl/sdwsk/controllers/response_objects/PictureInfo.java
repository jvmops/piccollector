package pl.sdwsk.controllers.response_objects;

/**
 * @author sdwsk
 */
public class PictureInfo {
    private String url;
    private String author;
    private String fileName;

    public PictureInfo(String url, String author, String fileName) {
        this.url = url;
        this.author = author;
        this.fileName = fileName;
    }

    public String getUrl() {
        return url;
    }

    public String getAuthor() {
        return author;
    }

    public String getFileName() {
        return fileName;
    }

    @Override
    public String toString() {
        return "PictureInfo{" +
                "url='" + url + '\'' +
                ", author='" + author + '\'' +
                ", fileName='" + fileName + '\'' +
                '}';
    }
}
