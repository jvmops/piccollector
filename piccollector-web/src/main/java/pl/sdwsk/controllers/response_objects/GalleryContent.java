package pl.sdwsk.controllers.response_objects;

import pl.sdwsk.model.Gallery;
import pl.sdwsk.model.Picture;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

/**
 * @author sdwsk
 */
public class GalleryContent implements Serializable
{
    private static final long serialVersionUID = -3996264332309121308L;

    private Gallery gallery;
    private Map<String, Collection<Picture>> picturesByAuthor;

    public GalleryContent(
            Gallery gallery,
            Map<String, Collection<Picture>> pictures)
    {
        this.gallery = gallery;
        this.picturesByAuthor = pictures;
    }

    public Gallery getGallery()
    {
        return gallery;
    }

    public Map<String, Collection<Picture>> getPicturesByAuthor()
    {
        return picturesByAuthor;
    }

    @Override
    public String toString()
    {
        return "GalleryContent{" +
                "gallery=" + gallery +
                ", picturesByAuthor=" + picturesByAuthor +
                '}';
    }
}
