package pl.sdwsk.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.sdwsk.model.Post;
import pl.sdwsk.services.NewsService;

import static pl.sdwsk.utils.NullUtils.defaultValueIfNull;

/**
 * @author sdwsk
 */
@Controller
public class News {
    private NewsService newsService;

    @Autowired
    public News(NewsService newsService)
    {
        this.newsService = newsService;
    }

    @RequestMapping("/")
    public String home(
            @RequestParam(required = false) Integer page,
            Model model)
    {
        page = defaultValueIfNull(page, 0);
        Page<Post> posts = newsService.getPosts(page);
        model.addAttribute("posts", posts);
        return "home";
    }


    @Secured("ROLE_ADMIN")
    @RequestMapping(
            value = "/postEditor",
            method = RequestMethod.GET)
    public String postEditor(@RequestParam(required = false) Long postId,
                             Authentication authentication,
                             Model model) {
        Post post;
        if (postId != null)
            post = newsService.getPost(postId);
        else
            post = new Post();
        model.addAttribute("post", post);
        return "postEditor";
    }

    @RequestMapping(
            value = "/savePost",
            method = RequestMethod.POST)
    public String savePost(
            @ModelAttribute("post") Post post)
    {
        newsService.save(post);
        return "redirect:/";
    }
}
