package pl.sdwsk.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.sdwsk.config.PicProperties;
import pl.sdwsk.repositories.GalleryRepository;
import pl.sdwsk.repositories.PictureRepository;
import pl.sdwsk.repositories.UserRepository;
import pl.sdwsk.utils.watchers.DirectoryWatcher;
import pl.sdwsk.utils.watchers.GalleryWatcher;
import pl.sdwsk.utils.watchers.PictureWatcher;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Entry point for syncing file system with database. File system events are
 * intercepted and handled by {@link DirectoryWatcher} implementations. There are
 * two watchers:
 *
 * {@link GalleryWatcher} listens for directory creation/deletion within a galleries root
 * folder. Galleries root directory is registered with this this watcher at application
 * startup.
 *
 * {@link PictureWatcher} is only concerned about pictures. Each gallery needs to register
 * it's directory with {@link PictureWatcher}. This service is exposing registration method
 * to the application context.
 *
 * @author sdwsk
 */
@Component
public class SynchronizationServiceImpl implements SynchronizationService
{
    private static Logger log = LoggerFactory.getLogger(SynchronizationService.class);

    private final ExecutorService executorService;
    private final DirectoryWatcher pictureWatcher;
    private final DirectoryWatcher galleryWatcher;

    @Autowired
    public SynchronizationServiceImpl(
            GalleryRepository galleryRepository,
            PictureRepository pictureRepository,
            UserRepository userRepository,
            PicProperties picProperties)
    {
        this.executorService = Executors.newFixedThreadPool(2);
        Path galleriesRoot = picProperties.getGalleriesRoot();
        this.pictureWatcher = new PictureWatcher(
                galleryRepository,
                pictureRepository,
                userRepository,
                galleriesRoot);
        registerGalleries(galleryRepository, galleriesRoot);
        executorService.execute(pictureWatcher);
        this.galleryWatcher = new GalleryWatcher(galleryRepository, pictureWatcher);
        galleryWatcher.register(galleriesRoot);
        executorService.execute(galleryWatcher);
    }

    /**
     * Registers given directory (gallery) with picture watcher.
     */
    @Override
    public void registerDirectory(Path directory)
    {
        pictureWatcher.register(directory);
        log.info("Directory registered with SynchronizationService {}", directory.toString());
    }

    /**
     * Register all gallery's directories with directory watcher.
     */
    private void registerGalleries(GalleryRepository galleryRepository, Path galleriesRoot)
    {
        Collection<String> galleryPaths = galleryRepository.findAllGalleryPaths();
        galleryPaths.stream()
                .map(Paths::get)
                .map(galleriesRoot::resolve)
                .forEach(this::registerDirectory);
    }

    @PreDestroy
    private void cleanUp() throws IOException
    {
        pictureWatcher.close();
        log.info("Closing picture watcher");
        galleryWatcher.close();
        log.info("Closing gallery watcher");
        executorService.shutdown();
        log.info("Shutting down executor service");
    }
}
