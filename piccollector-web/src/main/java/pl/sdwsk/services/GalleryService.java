package pl.sdwsk.services;

import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;
import pl.sdwsk.controllers.response_objects.GalleryContent;
import pl.sdwsk.controllers.response_objects.PictureInfo;
import pl.sdwsk.model.Gallery;

import java.nio.file.Path;
import java.time.LocalDate;

/**
 * Interface that is providing access to the galleries.
 *
 * @author sdwsk
 */
public interface GalleryService
{
    Gallery createGallery(String galleryName, LocalDate eventDate);
    Page<Gallery> getGalleries(int page);
    Gallery findGalleryByEventNameAndDate(String requestedEventName, LocalDate eventDate);
    GalleryContent galleryContent(Gallery gallery, String currentUsername);
    PictureInfo addPicture(MultipartFile uploadedFile, Gallery gallery, String author);
    PictureInfo setCover(Path fileName);
}