package pl.sdwsk.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pl.sdwsk.config.PicProperties;
import pl.sdwsk.controllers.response_objects.GalleryContent;
import pl.sdwsk.controllers.response_objects.PictureInfo;
import pl.sdwsk.exceptions.DuplicatedFileException;
import pl.sdwsk.exceptions.GalleryNotFoundException;
import pl.sdwsk.exceptions.PictureNotFoundException;
import pl.sdwsk.model.Gallery;
import pl.sdwsk.model.Picture;
import pl.sdwsk.repositories.GalleryRepository;
import pl.sdwsk.repositories.PictureRepository;
import pl.sdwsk.utils.FileUtils;
import pl.sdwsk.utils.GalleryUtils;
import pl.sdwsk.utils.PictureMetadata;
import pl.sdwsk.utils.UploadedFileProcessor;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Map;
import java.util.stream.StreamSupport;

import static pl.sdwsk.utils.GalleryResource.CONTENT_SEPARATOR;

/**
 * @author sdwsk
 */
@Service
public class GalleryServiceImpl implements GalleryService
{
    private static Logger log = LoggerFactory.getLogger(GalleryServiceImpl.class);
    private static final int GALLERIES_PAGE_SIZE = 12;
    private static final Sort GALLERIES_SORT_ORDER = new Sort(Sort.Direction.DESC, "eventDate");

    private GalleryRepository galleryRepository;
    private PictureRepository pictureRepository;
    private UploadedFileProcessor uploadedFileProcessor;
    private Path galleriesRoot;

    @Autowired
    public GalleryServiceImpl(
            GalleryRepository galleryRepository,
            PictureRepository pictureRepository,
            UploadedFileProcessor uploadedFileProcessor,
            PicProperties picProperties)
    {
        this.galleryRepository = galleryRepository;
        this.pictureRepository = pictureRepository;
        this.uploadedFileProcessor = uploadedFileProcessor;
        this.galleriesRoot = picProperties.getGalleriesRoot();
    }

    @Override
    public Gallery createGallery(String galleryName, LocalDate eventDate)
    {
        String galleryNameModified = galleryName.replace(" ", "-");
        Path directoryName = Paths.get(eventDate + CONTENT_SEPARATOR + galleryNameModified);
        Path absolutePath = galleriesRoot.resolve(directoryName);
        FileUtils.createDirectory(absolutePath);
        return GalleryUtils.createGalleryFrom(directoryName);
    }

    @Override
    public Page<Gallery> getGalleries(int page)
    {
        Pageable pageable = new PageRequest(page, GALLERIES_PAGE_SIZE, GALLERIES_SORT_ORDER);
        Page<Gallery> galleries = galleryRepository.findAll(pageable);
        StreamSupport.stream(galleries.spliterator(), false)
                .forEach(this::setPictureCount);
        return galleries;
    }

    private void setPictureCount(Gallery gallery)
    {
        int count = pictureRepository.countByGallery(gallery);
        gallery.setPictureCount(count);
    }

    @Override
    public Gallery findGalleryByEventNameAndDate(String requestedEventName, LocalDate eventDate)
    {
        String eventName = removeDashes(requestedEventName);
        return galleryRepository.findByNameAndEventDate(eventName, eventDate)
                .orElseThrow(() -> new GalleryNotFoundException(eventName, eventDate));
    }

    private String removeDashes(String galleryName)
    {
        return galleryName.replace("-", " ");
    }

    @Override
    public GalleryContent galleryContent(Gallery gallery, String currentUsername)
    {
        Collection<Picture> pictures = pictureRepository.findByGallery(gallery);
        Map<String, Collection<Picture>> picturesByAuthor = GalleryUtils.mapPicturesToAuthor(pictures, currentUsername);
        return new GalleryContent(gallery, picturesByAuthor);
    }

    @Override
    public PictureInfo addPicture(MultipartFile uploadedFile, Gallery gallery, String author)
    {
        long creationTime = PictureMetadata.getCreationTime(uploadedFile);
        Path newFileName = uploadedFileProcessor.resolveNewFilename(author, creationTime, FileUtils.getFileExtension(uploadedFile));
        if(pictureRepository.pictureExists(newFileName))
            throw new DuplicatedFileException(newFileName.toString());
        Path destinationPath = gallery.getPath().resolve(newFileName);
        String url = uploadedFileProcessor.transferPicture(uploadedFile, destinationPath);
        return new PictureInfo(url, author, newFileName.toString());
    }

    @Override
    public PictureInfo setCover(Path fileName)
    {
        Picture cover = pictureRepository.findByFileName(fileName)
                .orElseThrow(() -> new PictureNotFoundException(fileName));
        Gallery gallery = cover.getGallery();
        gallery.setCover(cover);
        galleryRepository.save(gallery);
        log.info("Picture {} set as cover", gallery.getPath().resolve(fileName));
        return new PictureInfo(
                cover.getUrl(),
                cover.getAuthor().getUsername(),
                cover.getFileName().toString());
    }
}
