package pl.sdwsk.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.sdwsk.controllers.form_objects.RegistrationDetails;
import pl.sdwsk.exceptions.UserNotFoundException;
import pl.sdwsk.model.User;
import pl.sdwsk.controllers.form_objects.ChangedPassword;
import pl.sdwsk.repositories.UserRepository;

import java.util.Optional;

/**
 * @author sdwsk
 */
@Service
public class UserServiceImpl implements UserService
{

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder)
    {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User findByEmail(String email)
    {
        return this.userRepository
                .findOneByEmailIgnoreCase(email)
                .orElseThrow(() -> new UserNotFoundException(email));
    }

    @Override
    public User register(RegistrationDetails registrationDetails)
    {
        User user = new User(registrationDetails);
        user.setPasswordHash(this.passwordEncoder.encode(registrationDetails.getPassword()));
        return this.userRepository.save(user);
    }

    @Override
    public User changePass(ChangedPassword changedPassword)
    {
        User user = userRepository.findByUsernameIgnoreCase(changedPassword.getUserName())
                .orElseThrow(() -> new UserNotFoundException(changedPassword.getUserName()));
        user.setPasswordHash(passwordEncoder.encode(changedPassword.getNewPassword()));
        return this.userRepository.save(user);
    }
}
