package pl.sdwsk.services;

import pl.sdwsk.controllers.form_objects.ChangedPassword;
import pl.sdwsk.controllers.form_objects.RegistrationDetails;
import pl.sdwsk.model.User;

/**
 * @author sdwsk
 */
public interface UserService
{
    User findByEmail(String email);
    User register(RegistrationDetails form);
    User changePass(ChangedPassword changedPassword);
}
