package pl.sdwsk.services;

import org.springframework.data.domain.Page;
import org.springframework.security.access.annotation.Secured;
import pl.sdwsk.model.Post;

/**
 * This service retrieves posts to be rendered on home page.
 *
 * @author sdwsk
 */
public interface NewsService {
    Page<Post> getPosts(int pageNumber);

    @Secured("ROLE_ADMIN")
    Post getPost(Long postId);

    @Secured("ROLE_ADMIN")
    Post save(Post post);
}
