package pl.sdwsk.services;

import java.nio.file.Path;

/**
 * @author sdwsk
 */
public interface SynchronizationService
{
    void registerDirectory(Path directory);
}
