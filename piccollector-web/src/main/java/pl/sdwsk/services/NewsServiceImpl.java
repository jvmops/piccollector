package pl.sdwsk.services;

import com.github.rjeschke.txtmark.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pl.sdwsk.exceptions.PostNotFoundException;
import pl.sdwsk.model.Post;
import pl.sdwsk.repositories.PostRepository;

import java.time.LocalDate;
import java.util.Optional;

/**
 * @author sdwsk
 */
@Service
public class NewsServiceImpl implements NewsService {
    private PostRepository postRepository;

    @Autowired
    public NewsServiceImpl(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    @Override
    public Page<Post> getPosts(int pageNumber)
    {
        int pageSize = 10;
        Sort.Order sticked = new Sort.Order(Sort.Direction.DESC, "sticked");
        Sort.Order byDate = new Sort.Order(Sort.Direction.DESC, "creationDate");
        Sort sort = new Sort(sticked, byDate);
        Page<Post> posts = postRepository.findAll(new PageRequest(pageNumber, pageSize, sort));
        for (Post post : posts)
            transformMarkdownIntoHtml(post);
        return posts;
    }

    private void transformMarkdownIntoHtml(Post post)
    {
        String markdown = post.getContent();
        String html = Processor.process(markdown);
        post.setContent(html);
    }

    @Override
    public Post getPost(Long postId)
    {
        return postRepository.findById(postId)
                .orElseThrow(() -> new PostNotFoundException(postId));
    }

    @Override
    public Post save(Post post)
    {
        if(post.getId() == null)
            post.setCreationDate(LocalDate.now());
        return postRepository.save(post);
    }
}
