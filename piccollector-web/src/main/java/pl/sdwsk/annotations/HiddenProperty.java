package pl.sdwsk.annotations;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation lets Thymeleaf ignore chosen fields in model. It is necessary to avoid
 * infinite recursion during rendering a page.
 *
 * @author Gui Meira at StackOverflow
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@JacksonAnnotationsInside
@JsonIgnore
public @interface HiddenProperty {}
