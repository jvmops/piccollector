package pl.sdwsk.annotations;

import pl.sdwsk.annotations.HiddenPropertyBeanInfo;
import pl.sdwsk.model.Picture;

/**
 * Implementation of {@link pl.sdwsk.annotations.HiddenProperty } for Picture class.
 *
 * @author sdwsk
 */
public class PictureBeanInfo extends HiddenPropertyBeanInfo {
    public PictureBeanInfo() {
        super(Picture.class);
    }
}
