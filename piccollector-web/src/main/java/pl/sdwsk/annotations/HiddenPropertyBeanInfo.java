package pl.sdwsk.annotations;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.beans.SimpleBeanInfo;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Gui Meira at StackOverflow
 */
public class HiddenPropertyBeanInfo extends SimpleBeanInfo {
    private Class<?> beanClass;
    private PropertyDescriptor[] descriptors;

    public HiddenPropertyBeanInfo(Class<?> beanClass)
    {
        this.beanClass = beanClass;
    }

    @Override
    public PropertyDescriptor[] getPropertyDescriptors()
    {
        if(descriptors != null)
        {
            return descriptors;
        }

        Method[] methodList = beanClass.getMethods();
        List<PropertyDescriptor> propDescriptors = new ArrayList<>();

        for(Method m : methodList) {
            if(Modifier.isStatic(m.getModifiers()))
                continue;

            try
            {
                if(m.getParameterCount() == 0 && !m.isAnnotationPresent(HiddenProperty.class))
                {
                    if(m.getName().startsWith("get"))
                    {
                        String propertyName = m.getName().substring(3);
                        propDescriptors.add(new PropertyDescriptor(lowerFirstLetter(propertyName), beanClass));
                    }
                    else if(m.getName().startsWith("is"))
                    {
                        String propertyName = m.getName().substring(2);
                        propDescriptors.add(new PropertyDescriptor(lowerFirstLetter(propertyName), beanClass));
                    }
                }
            }
            catch(IntrospectionException ex)
            {
                continue;
            }
        }

        descriptors = new PropertyDescriptor[propDescriptors.size()];
        return propDescriptors.toArray(descriptors);
    }

    private String lowerFirstLetter(String propertyName)
    {
        char c[] = propertyName.toCharArray();
        c[0] = Character.toLowerCase(c[0]);
        return new String(c);
    }
}
