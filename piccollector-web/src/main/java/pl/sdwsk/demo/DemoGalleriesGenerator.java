package pl.sdwsk.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import pl.sdwsk.config.PicProperties;
import pl.sdwsk.repositories.GalleryRepository;
import pl.sdwsk.services.SynchronizationService;
import pl.sdwsk.utils.FileUtils;
import pl.sdwsk.utils.GalleryResource;
import pl.sdwsk.utils.GalleryUtils;
import pl.sdwsk.utils.PictureMetadata;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static pl.sdwsk.utils.GalleryResource.CONTENT_SEPARATOR;

/**
 * Class responsible for generating demo galleries if there is none provided at root
 * galleries directory.
 *
 * @author sdwsk
 */
@Configuration
@Profile("demo")
public class DemoGalleriesGenerator
{
    private static Logger log = LoggerFactory.getLogger(DemoGalleriesGenerator.class);
    private static final Random RAND = new Random();
    private static final List<String> AUTHORS = Arrays.asList("Yenn", "Geralt", "Priscilla");

    private SynchronizationService synchronizationService;
    private GalleryRepository galleryRepository;
    private DefaultUsers defaultUsers;
    private Path galleriesRoot;

    @Autowired
    public void setSynchronizationService(SynchronizationService synchronizationService)
    {
        this.synchronizationService = synchronizationService;
    }

    @Autowired
    public void setGalleryRepository(GalleryRepository galleryRepository)
    {
        this.galleryRepository = galleryRepository;
    }

    @Autowired
    public void setDefaultUsers(DefaultUsers defaultUsers)
    {
        this.defaultUsers = defaultUsers;
    }

    @Autowired
    public void setGalleriesRoot(PicProperties piccollectorProps)
    {
        this.galleriesRoot = piccollectorProps.getGalleriesRoot();
    }

    @PostConstruct
    private void generateGalleriesIfNonePresent() throws IOException
    {
        if (Files.notExists(this.galleriesRoot))
        {
            Files.createDirectory(this.galleriesRoot);
            log.info("Galleries root directory created: {}", this.galleriesRoot.toString());
        }
        boolean noGalleriesAvailable = isDirEmpty(this.galleriesRoot);
        defaultUsers.setupUsers();
        if (noGalleriesAvailable) {
            log.info("There is no galleries available at {}. Attempting to generate demo galleries.", this.galleriesRoot.toString());
            List<Path> galleryPaths = resolveDemoPaths();
            generateDemoGalleries(galleryPaths);
        }
    }

    /**
     * This method ensures that there is no gallery in galleries root directory.
     */
    private boolean isDirEmpty(Path directory) throws IOException
    {
        try(DirectoryStream<Path> dirStream = Files.newDirectoryStream(directory)) {
            return !dirStream.iterator().hasNext();
        }
    }

    /**
     * @return absolute paths for demo gallery's directories
     */
    private List<Path> resolveDemoPaths()
    {
        return Arrays.asList(this.galleriesRoot.resolve("2016-05-26_Piccollector-birthday"),
                this.galleriesRoot.resolve("2016-05-29_First-steps"),
                this.galleriesRoot.resolve("2016-06-01_Localhost-trip"),
                this.galleriesRoot.resolve("2016-06-28_Most-recent-gallery"));
    }

    /**
     * Each provided path is turned into a gallery. Directory is created
     * and populated with allPictures.
     */
    private void generateDemoGalleries(List<Path> galleryPaths)
    {
        PlaceHoldItClient pictureService = new PlaceHoldItClient();
        for (Path demoGallery : galleryPaths)
        {
            FileUtils.createDirectory(demoGallery);
            GalleryUtils.saveGallery(demoGallery, this.galleryRepository);
            this.synchronizationService.registerDirectory(demoGallery);
            populateGallery(demoGallery, pictureService);
        }
        pictureService.clearCache();
    }

    /**
     * This method populates given directory with random number of allPictures for every
     * author. Filename pattern is {author}_{creationTime}.{extension}.
     */
    private void populateGallery(Path gallery, PlaceHoldItClient pictureService)
    {
        for (String author : AUTHORS)
        {
            int numberOfPictures = getRandomNumber();
            for (int i = 0; i < numberOfPictures; i++)
            {
                byte[] picture = pictureService.downloadPicture();
                String expectedFilename = author + CONTENT_SEPARATOR + System.currentTimeMillis() + ".png";
                Path picturePath = Paths.get(expectedFilename);
                Path absolutePicturePath = gallery.resolve(picturePath);
                save(picture, absolutePicturePath);
            }
        }
    }

    /**
     * This method resolver random number of allPictures with a range of 5-19 for a given author.
     */
    private int getRandomNumber()
    {
        return RAND.nextInt(15) + 5;
    }

    /**
     * This method saves picture at the given path and handles the exception.
     */
    private void save(byte[] picture, Path picturePath)
    {
        try
        {
            Files.write(picturePath, picture);
            log.info("Picture downloaded and saved as: {}", picturePath.toString());
        }
        catch (IOException e)
        {
            log.error("Error during saving picture at this path: {}", picturePath, e);
        }
    }
}
