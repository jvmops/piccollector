package pl.sdwsk.demo;

import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Client for PlaceHold.it
 *
 * @author sdwsk
 */
class PlaceHoldItClient
{
    private static final String PICTURE_SERVICE_URL = "https://placehold.it/";
    private static final List<String> PICTURE_SIZES= Arrays.asList("800x600", "1250x920", "1080x1920", "900x640",
            "1600x1200", "1920x1080", "800x1440", "1920x1200", "1440x1050", "1200x600", "720x1240");
    private static final HashMap<String, byte[]> PICTURE_CACHE = new HashMap<>();
    private static final RestTemplate restTemplate = new RestTemplate();
    private static final Random rand = new Random();

    /**
     * If picture of a given size is in the PICTURE_CACHE this method returns this picture.
     * Otherwise it downloads this picture from PlaceHold.it, puts it in a cache and returns.
     *
     * @return picture
     */
    byte[] downloadPicture()
    {
        String pictureSize = getRandomPictureSize();
        if (PICTURE_CACHE.containsKey(pictureSize))
            return PICTURE_CACHE.get(pictureSize);

        String pictureUrl = PICTURE_SERVICE_URL + pictureSize;
        byte[] picture = restTemplate.getForObject(pictureUrl, byte[].class);
        PICTURE_CACHE.put(pictureSize, picture);
        return picture;
    }

    void clearCache()
    {
        PICTURE_CACHE.clear();
    }

    /**
     * Randomly picks picture size from a specified list.
     *
     * @return String representation of picture size
     */
    private String getRandomPictureSize()
    {
        int size = PICTURE_SIZES.size();
        return PICTURE_SIZES.get(rand.nextInt(size));
    }
}
