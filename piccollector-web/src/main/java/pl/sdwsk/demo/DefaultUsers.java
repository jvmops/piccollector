package pl.sdwsk.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import pl.sdwsk.model.User;
import pl.sdwsk.repositories.UserRepository;

/**
 * Class is delievering two sample users for demo purposes
 *
 * @author sdwsk
 */
@Component
public class DefaultUsers
{
    private UserRepository userRepository;
    private PasswordEncoder encoder;

    @Autowired
    public DefaultUsers(UserRepository userRepository, PasswordEncoder encoder) {
        this.userRepository = userRepository;
        this.encoder = encoder;
    }

    void setupUsers()
    {
        if (!userRepository.findByUsernameIgnoreCase("admin").isPresent())
            createAdmin();
        if (!userRepository.findByUsernameIgnoreCase("yenn").isPresent())
            createYenn();
        if (!userRepository.findByUsernameIgnoreCase("geralt").isPresent())
            createGeralt();
        if (!userRepository.findByUsernameIgnoreCase("priscilla").isPresent())
            createPriscilla();
        if (!userRepository.findByUsernameIgnoreCase("user").isPresent())
            createUser();
    }

    private void createAdmin()
    {
        User admin = new User();
        admin.setUsername("Admin");
        admin.setEmail("admin");
        admin.setPasswordHash(encoder.encode("pass"));
        admin.setRole(User.Role.ROLE_ADMIN);
        userRepository.save(admin);
    }

    private void createUser()
    {
        User user = new User();
        user.setUsername("User");
        user.setEmail("user");
        user.setPasswordHash(encoder.encode("pass"));
        user.setRole(User.Role.ROLE_USER);
        userRepository.save(user);
    }

    private void createYenn()
    {
        User yenn = new User();
        yenn.setUsername("Yenn");
        yenn.setEmail("yenn");
        yenn.setPasswordHash(encoder.encode("pass"));
        yenn.setRole(User.Role.ROLE_USER);
        userRepository.save(yenn);
    }

    private void createGeralt()
    {
        User geralt = new User();
        geralt.setUsername("Geralt");
        geralt.setEmail("geralt");
        geralt.setPasswordHash(encoder.encode("pass"));
        geralt.setRole(User.Role.ROLE_USER);
        userRepository.save(geralt);
    }

    private void createPriscilla()
    {
        User priscilla = new User();
        priscilla.setUsername("Priscilla");
        priscilla.setEmail("priscilla");
        priscilla.setPasswordHash(encoder.encode("pass"));
        priscilla.setRole(User.Role.ROLE_USER);
        userRepository.save(priscilla);
    }
}
