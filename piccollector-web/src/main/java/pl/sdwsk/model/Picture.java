package pl.sdwsk.model;

import pl.sdwsk.annotations.HiddenProperty;

import javax.persistence.*;
import java.nio.file.Path;

/**
 * @author sdwsk
 */
@Entity
public class Picture
{
    private static final long serialVersionUID = 8795121971172038179L;

    long id;
    private Gallery gallery;
    private User author;
    private long size;
    private String url;
    private Path fileName;

    public Picture() {}

    public Picture(Gallery gallery, User author, long size, String url, Path fileName)
    {
        this.gallery = gallery;
        this.author = author;
        this.size = size;
        this.url = url;
        this.fileName = fileName;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name="GALLERY_ID", nullable = false)
    @HiddenProperty
    public Gallery getGallery()
    {
        return gallery;
    }

    public void setGallery(Gallery gallery)
    {
        this.gallery = gallery;
    }

    @ManyToOne
    @JoinColumn(name="AUTHOR_ID", nullable = false)
    @HiddenProperty
    public User getAuthor()
    {
        return author;
    }

    public void setAuthor(User author)
    {
        this.author = author;
    }

    @Column(nullable = false)
    public long getSize()
    {
        return size;
    }

    public void setSize(long size)
    {
        this.size = size;
    }

    @Column(nullable = false)
    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    @Column(nullable = false)
    public Path getFileName()
    {
        return fileName;
    }

    public void setFileName(Path fileName)
    {
        this.fileName = fileName;
    }

    @Override
    public String toString()
    {
        return "Picture{" +
                "url='" + url + '\'' +
                ", size=" + size +
                ", id=" + id +
                '}';
    }
}
