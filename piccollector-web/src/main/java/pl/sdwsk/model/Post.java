package pl.sdwsk.model;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.time.LocalDate;

/**
 * @author sdwsk
 */
@Entity
public class Post extends AbstractPersistable<Long>
{
    private static final long serialVersionUID = -5872709159150052956L;

    private LocalDate creationDate;
    private String title;
    private String content;
    private boolean sticked;

    @Column(nullable = false, updatable = false)
    public LocalDate getCreationDate()
    {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate)
    {
        this.creationDate = creationDate;
    }

    @Column(length = 2000, nullable = false)
    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    @Column(nullable = false)
    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    @Column(nullable = true, updatable = false)
    public boolean isSticked()
    {
        return sticked;
    }

    public void setSticked(boolean sticked)
    {
        this.sticked = sticked;
    }

    @Override
    public void setId(Long id)
    {
        super.setId(id);
    }

    @Override
    public Long getId()
    {
        return super.getId();
    }


}
