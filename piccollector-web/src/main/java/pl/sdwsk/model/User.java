package pl.sdwsk.model;

import org.springframework.data.jpa.domain.AbstractPersistable;
import pl.sdwsk.controllers.form_objects.RegistrationDetails;

import javax.persistence.*;

/**
 * @author sdwsk
 */
@Entity
@Table(name = "USERS")
public class User extends AbstractPersistable<Long>
{
    private static final long serialVersionUID = 4501940602659740058L;

    public static enum Role
    {
        ROLE_ADMIN, ROLE_MOD, ROLE_USER
    }

    private String username;
    private String email;
    private String passwordHash;
    private Role role;

    public User() {}

    public User(RegistrationDetails registrationDetails)
    {
        this.email = registrationDetails.getEmail();
        this.username = registrationDetails.getUsername();
        this.role = registrationDetails.getRole();
    }

    @Column(nullable = false, unique = true)
    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    @Column(nullable = false, unique = true)
    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    @Column(nullable = false)
    public String getPasswordHash()
    {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash)
    {
        this.passwordHash = passwordHash;
    }

    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    public Role getRole()
    {
        return role;
    }

    public void setRole(Role role)
    {
        this.role = role;
    }


}
