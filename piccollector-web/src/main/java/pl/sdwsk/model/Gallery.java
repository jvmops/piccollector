package pl.sdwsk.model;

import javax.persistence.*;
import java.io.Serializable;
import java.nio.file.Path;
import java.time.LocalDate;

/**
 * @author sdwsk
 */
@Entity
public class Gallery implements Serializable
{
    private static final long serialVersionUID = 5052072024636279537L;

    private long id;
    private String name;
    private LocalDate eventDate;
    private Path path;
    private String url;
    private Picture cover;
    private int pictureCount;

    public Gallery() {}

    public Gallery(String name, LocalDate eventDate, Path path, String url)
    {
        this.name = name;
        this.eventDate = eventDate;
        this.path = path;
        this.url = url;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public LocalDate getEventDate()
    {
        return eventDate;
    }

    public void setEventDate(LocalDate eventDate)
    {
        this.eventDate = eventDate;
    }

    public Path getPath()
    {
        return path;
    }

    public void setPath(Path path)
    {
        this.path = path;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    @OneToOne
    @JoinColumn
    public Picture getCover()
    {
        return cover;
    }

    public void setCover(Picture cover)
    {
        this.cover = cover;
    }

    @Transient
    public int getPictureCount()
    {
        return pictureCount;
    }

    public void setPictureCount(int pictureCount)
    {
        this.pictureCount = pictureCount;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Gallery gallery = (Gallery) o;

        return id == gallery.id;
    }

    @Override
    public int hashCode()
    {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString()
    {
        return "Gallery{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", eventDate=" + eventDate +
            ", path=" + path +
            ", url='" + url + '\'' +
            ", cover=" + (cover != null ? cover.getId() : "none") +
            '}';
    }
}
