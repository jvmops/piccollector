package pl.sdwsk.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.sdwsk.controllers.form_objects.ChangedPassword;
import pl.sdwsk.exceptions.UserNotFoundException;
import pl.sdwsk.model.User;
import pl.sdwsk.repositories.UserRepository;

import java.util.Optional;

/**
 * @author sdwsk
 */
@Component
public class ChangedPasswordValidator implements Validator
{
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public ChangedPasswordValidator(UserRepository userRepository, PasswordEncoder passwordEncoder)
    {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public boolean supports(Class<?> clazz)
    {
        return clazz.equals(ChangedPassword.class);
    }

    @Override
    public void validate(Object target, Errors errors)
    {
        ChangedPassword changedPassword = (ChangedPassword) target;
        isOldPasswordValid(errors, changedPassword);
        isPasswordConfirmed(errors, changedPassword);
    }

    private void isOldPasswordValid(Errors errors, ChangedPassword changedPassword)
    {
        User user = userRepository.findByUsernameIgnoreCase(changedPassword.getUserName())
                .orElseThrow(() -> new UserNotFoundException(changedPassword.getUserName()));
        String storedPassHash = user.getPasswordHash();
        if (!passwordEncoder.matches(changedPassword.getOldPassword(), storedPassHash))
            errors.rejectValue("oldPassword", "password.incorrect");
    }

    private void isPasswordConfirmed(Errors errors, ChangedPassword form)
    {
        if (!form.getNewPassword().equals(form.getPasswordConfirmation()))
            errors.rejectValue("passwordConfirmation", "password.do_not_match");
    }
}
