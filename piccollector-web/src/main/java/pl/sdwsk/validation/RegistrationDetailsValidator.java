package pl.sdwsk.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.sdwsk.controllers.form_objects.RegistrationDetails;
import pl.sdwsk.repositories.UserRepository;

/**
 * @author sdwsk
 */
@Component
public class RegistrationDetailsValidator implements Validator
{

    private UserRepository userRepository;

    @Autowired
    public RegistrationDetailsValidator(UserRepository userRepository)
    {
        this.userRepository = userRepository;
    }

    @Override
    public boolean supports(Class<?> clazz)
    {
        return clazz.equals(RegistrationDetails.class);
    }

    @Override
    public void validate(Object target, Errors errors)
    {
        RegistrationDetails form = (RegistrationDetails) target;
        isPasswordConfirmed(errors, form);
        isEmailUnique(errors, form);
        isUsernameUnique(errors, form);
    }

    private void isPasswordConfirmed(Errors errors, RegistrationDetails registrationDetails)
    {
        if (!passwordConfirmed(registrationDetails))
            errors.rejectValue("passwordConfirmation", "password.do_not_match");
    }

    private boolean passwordConfirmed(RegistrationDetails registrationDetails)
    {
        return registrationDetails.getPassword().equals(registrationDetails.getPasswordConfirmation());
    }

    private void isEmailUnique(Errors errors, RegistrationDetails registrationDetails)
    {
        if(userRepository.existsByEmail(registrationDetails.getEmail()))
            errors.rejectValue("email", "email.exists");
    }

    private void isUsernameUnique(Errors errors, RegistrationDetails registrationDetails) {
        if(userRepository.existsByUsername(registrationDetails.getUsername()))
            errors.rejectValue("username", "username.exists");
    }
}
