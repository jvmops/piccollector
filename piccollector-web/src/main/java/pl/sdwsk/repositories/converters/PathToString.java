package pl.sdwsk.repositories.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Converter for JPA model.
 *
 * @author sdwsk
 */
@Converter(autoApply = true)
public class PathToString implements AttributeConverter<Path, String>
{

    @Override
    public String convertToDatabaseColumn(Path source)
    {
        return (source == null ? null : source.toString());
    }

    @Override
    public Path convertToEntityAttribute(String source)
    {
        return source == null?null : Paths.get(source);
    }
}
