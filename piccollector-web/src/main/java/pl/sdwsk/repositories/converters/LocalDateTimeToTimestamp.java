package pl.sdwsk.repositories.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * Converter for JPA model.
 *
 * @author sdwsk
 */
@Converter(autoApply = true)
public class LocalDateTimeToTimestamp implements AttributeConverter<LocalDateTime, Timestamp>
{

    @Override
    public Timestamp convertToDatabaseColumn(LocalDateTime localDateTime)
    {
        return (localDateTime == null ? null : Timestamp.valueOf(localDateTime));
    }

    @Override
    public LocalDateTime convertToEntityAttribute(Timestamp timestamp)
    {
        return timestamp == null?null : timestamp.toLocalDateTime();
    }
}
