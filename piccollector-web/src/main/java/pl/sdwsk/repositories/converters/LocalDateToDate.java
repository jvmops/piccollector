package pl.sdwsk.repositories.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Date;
import java.time.LocalDate;

/**
 * Converter for JPA model.
 *
 * @author sdwsk
 */
@Converter(autoApply = true)
public class LocalDateToDate implements AttributeConverter<LocalDate, Date>
{

    @Override
    public Date convertToDatabaseColumn(LocalDate localDate)
    {
        return (localDate == null ? null : Date.valueOf(localDate));
    }

    @Override
    public LocalDate convertToEntityAttribute(Date date)
    {
        return date == null?null: date.toLocalDate();
    }
}