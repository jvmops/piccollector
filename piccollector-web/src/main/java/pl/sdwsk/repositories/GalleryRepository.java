package pl.sdwsk.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import pl.sdwsk.model.Gallery;

import java.nio.file.Path;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Optional;

/**
 * @author sdwsk
 */
@org.springframework.stereotype.Repository
public interface GalleryRepository extends Repository<Gallery, Long>
{
    Gallery save(Gallery gallery);
    Page<Gallery> findAll(Pageable pageable);
    Optional<Gallery> findByNameAndEventDate(String galleryName, LocalDate eventDate);
    Optional<Gallery> findById(long galleryId);

    @Query(value = "SELECT g.path FROM gallery g", nativeQuery = true)
    Collection<String> findAllGalleryPaths();

    @Query("SELECT g.id FROM Gallery g WHERE path = :galleryPath")
    Optional<Long> findIdByPath(@Param("galleryPath") Path galleryPath);
}
