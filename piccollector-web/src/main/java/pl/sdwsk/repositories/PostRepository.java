package pl.sdwsk.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.Repository;
import pl.sdwsk.model.Post;

import java.util.Optional;

/**
 * @author sdwsk
 */
@org.springframework.stereotype.Repository
public interface PostRepository extends Repository<Post, Long>
{
    Post save(Post post);
    Page<Post> findAllBySticked(Pageable pageable, boolean sticked);
    Optional<Post> findById(Long postId);
    Optional<Post> findFirtByStickedTrue();

    Page<Post> findAll(Pageable pageable);
}
