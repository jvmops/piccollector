package pl.sdwsk.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import pl.sdwsk.model.Gallery;
import pl.sdwsk.model.Picture;
import pl.sdwsk.model.User;
import pl.sdwsk.security.PiccollectorUser;

import java.nio.file.Path;
import java.util.Collection;
import java.util.Optional;

/**
 * @author sdwsk
 */
@org.springframework.stereotype.Repository
public interface PictureRepository extends Repository<Picture, Long>
{
    Picture save(Picture picture);
    Optional<Picture> findById(long id);
    Optional<Picture> findByFileName(Path fileName);

    @Query("SELECT count(p) FROM Picture p WHERE p.gallery = :gallery")
    int countByGallery(@Param("gallery") Gallery gallery);

    @Query("SELECT p FROM Picture p WHERE p.gallery = :gallery")
    Collection<Picture> findByGallery(@Param("gallery") Gallery gallery);

    @Query("SELECT count(p) > 0 FROM Picture p WHERE p.fileName = :fileName")
    boolean pictureExists(@Param("fileName") Path fileName);
}
