package pl.sdwsk.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import pl.sdwsk.model.User;

import java.util.Optional;

/**
 * @author sdwsk
 */
@org.springframework.stereotype.Repository
public interface UserRepository extends Repository<User, Long>
{
    User save(User user);
    Optional<User> findByUsername(String author);
    Optional<User> findByUsernameIgnoreCase(String username);
    Optional<User> findOneByEmailIgnoreCase(String email);

    @Query("SELECT count(u)>0 FROM User u WHERE upper(u.email) = upper(:email)")
    boolean existsByEmail(@Param("email") String email);

    @Query("SELECT count(u)>0 FROM User u WHERE upper(u.username) = upper(:username)")
    boolean existsByUsername(@Param("username") String username);
}
