package pl.sdwsk.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.nio.file.Path;
import java.time.LocalDate;

/**
 * @author sdwsk
 */
@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Gallery not found!")
public class GalleryNotFoundException extends RuntimeException
{
    private static final long serialVersionUID = 4224746266518039184L;

    public GalleryNotFoundException(String eventName, LocalDate eventDate)
    {
        super(String.format("Requested gallery not found. %s (%s)", eventName, eventDate.toString()));
    }

    public GalleryNotFoundException(long galleryId)
    {
        super(String.format("Gallery with id: %d not found.", galleryId));
    }


    public GalleryNotFoundException(Path galleryPath)
    {
        super(String.format("There is no gallery saved under: %s", galleryPath.toString()));
    }
}
