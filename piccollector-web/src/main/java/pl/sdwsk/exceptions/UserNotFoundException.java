package pl.sdwsk.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author sdwsk
 */
@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR, reason = "User not found.")
public class UserNotFoundException extends RuntimeException
{
    private static final long serialVersionUID = -7783983452409697128L;

    public UserNotFoundException(String lookUpArg)
    {
        super(String.format("User not found. Looked up by: %s", lookUpArg));
    }

    public UserNotFoundException(long authorId)
    {
        super(String.format("User with id: %d not found", authorId));
    }
}
