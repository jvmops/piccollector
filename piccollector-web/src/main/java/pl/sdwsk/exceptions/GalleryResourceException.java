package pl.sdwsk.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author sdwsk
 */
@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Server can not process your request.")
public class GalleryResourceException extends RuntimeException
{
    private static final long serialVersionUID = -8442858635526622806L;

    public GalleryResourceException(String message)
    {
        super(message);
    }
}
