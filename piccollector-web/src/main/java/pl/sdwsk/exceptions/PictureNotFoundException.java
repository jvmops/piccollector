package pl.sdwsk.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.sdwsk.model.Picture;

import java.nio.file.Path;

/**
 * @author sdwsk
 */
@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Picture not found.")
public class PictureNotFoundException extends RuntimeException
{
    private static final long serialVersionUID = 2138851788323324449L;

    public PictureNotFoundException(long pictureId)
    {
        super(String.format("Picture with id: %d not found", pictureId));
    }

    public PictureNotFoundException(Path fileName) {
        super(String.format("Picture with name: %s not found", fileName.toString()));
    }
}
