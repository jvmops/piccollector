package pl.sdwsk.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author sdwsk
 */

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Post not found.")
public class PostNotFoundException extends RuntimeException
{
    private static final long serialVersionUID = 5530365561882855645L;

    public PostNotFoundException(Long postId)
    {
        super(String.format("Post with id: %d not found.", postId));
    }
}
