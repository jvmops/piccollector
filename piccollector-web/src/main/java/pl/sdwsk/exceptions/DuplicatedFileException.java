package pl.sdwsk.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author sdwsk
 */
@ResponseStatus(code = HttpStatus.CONFLICT, reason = "You have uploaded this file before")
public class DuplicatedFileException extends RuntimeException
{
    private static final long serialVersionUID = 1822633610973291348L;

    public DuplicatedFileException(String fileName)
    {
        super(String.format("Uploaded file already exists in database: %s", fileName));
    }
}
