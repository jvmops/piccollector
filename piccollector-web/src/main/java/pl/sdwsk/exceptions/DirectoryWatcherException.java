package pl.sdwsk.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author sdwsk
 */
@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Server can not process your request.")
public class DirectoryWatcherException extends RuntimeException
{
    private static final long serialVersionUID = 8436224804541396273L;

    public DirectoryWatcherException(String message)
    {
        super(message);
    }

    public DirectoryWatcherException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
