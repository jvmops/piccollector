package pl.sdwsk.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.IOException;
import java.nio.file.Path;

/**
 * @author sdwsk
 */
@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Server can not process your file.")
public class FileUploadException extends RuntimeException
{
    private static final long serialVersionUID = 1822633610973291348L;

    public FileUploadException(String originalFilename, Path destination, IOException e)
    {
        super(String.format("Error during transferring %s to %s",
                originalFilename, destination.toString()), e);
    }

    public FileUploadException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
