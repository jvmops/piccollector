package pl.sdwsk.groovy.integration

import com.drew.imaging.ImageProcessingException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.core.io.ClassPathResource
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.context.TestPropertySource
import org.springframework.test.context.web.WebAppConfiguration
import pl.sdwsk.Piccollector
import pl.sdwsk.exceptions.FileUploadException
import pl.sdwsk.groovy.integration.configuration.TempDir
import pl.sdwsk.model.Gallery
import pl.sdwsk.repositories.GalleryRepository
import pl.sdwsk.repositories.PictureRepository
import pl.sdwsk.services.GalleryService
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise

import java.nio.file.Paths
import java.time.LocalDate
import java.time.format.DateTimeParseException

/**
 * @author sdwsk
 */
@SpringApplicationConfiguration(classes = [Piccollector, TempDir])
@TestPropertySource(locations="classpath:test.properties")
@WebAppConfiguration
@Stepwise
class GalleryServiceSpec extends Specification {

    @Autowired private GalleryService galleryService;
    @Autowired private GalleryRepository galleryRepository;
    @Autowired private PictureRepository pictureRepository;

    def @Shared eventName = "Test gallery"
    def @Shared eventDate = LocalDate.parse("2016-05-01")
    def @Shared pageNumber = 0
    def @Shared index = 0
    def @Shared author = "User"

    def "created gallery will be persisted into database" () {
        when:
        def gallery = galleryService.createGallery(eventName, eventDate)
        // wait for an event to be processed
        Thread.sleep(500)

        then:
        def retrievedGallery = galleryRepository.findByNameAndEventDate(eventName, eventDate)
        retrievedGallery.present
        retrievedGallery.get().path == Paths.get("2016-05-01_Test-gallery")
    }

    def "information about galleries should be retrieved as page" () {
        when:
        def galleries = galleryService.getGalleries(pageNumber)
        def gallery = galleries.getContent().get(index)

        then:
        galleries.totalPages == 1
        gallery.name == eventName
        gallery.eventDate == eventDate
    }

    def "picture count should be set after galleries are retrieved" () {
        when:
        def galleries = galleryService.getGalleries(pageNumber)
        def gallery = galleries.content.get(index)

        then:
        gallery.pictureCount == 0
    }

    def "uploaded file must be image" () {
        given:
        def multipartFile = new MockMultipartFile("samplePicture.jpg", "samplePicture.jpg", "image/*", "asd".getBytes());
        def gallery = galleryService.findGalleryByEventNameAndDate(eventName, eventDate)

        when:
        galleryService.addPicture(multipartFile, gallery, author)

        then:
        thrown(FileUploadException)
    }

    def "uploaded picture will be persisted into database" () {
        given:
        def resource = new ClassPathResource("samplePicture.jpg");
        def multipartFile = new MockMultipartFile("samplePicture.jpg", "samplePicture.jpg", "image/*", resource.getInputStream());
        def gallery = galleryService.findGalleryByEventNameAndDate(eventName, eventDate)

        when:
        galleryService.addPicture(multipartFile, gallery, author)
        // wait for an event to be processed
        Thread.sleep(500)
        gallery = galleryService.galleryContent(gallery, author)

        then:
        gallery.picturesByAuthor.containsKey(author)

        and:
        def userPictures = gallery.picturesByAuthor.get(author)
        userPictures.size == 1

        and:
        def retrievedPicture = userPictures.get(index)
        retrievedPicture.fileName.toString().matches("User_(\\d{13}).jpg")
    }

    def "setting cover for albums" () {
        given:
        def gallery = galleryService.findGalleryByEventNameAndDate(eventName, eventDate)
        def pictures = pictureRepository.findByGallery(gallery)
        def picture = pictures.stream().findFirst().get()
        def fileName = picture.fileName

        when:
        galleryService.setCover(fileName)
        gallery = galleryService.findGalleryByEventNameAndDate(eventName, eventDate)

        then:
        gallery.cover.fileName.toString().matches("User_(\\d{13}).jpg")
    }
}
