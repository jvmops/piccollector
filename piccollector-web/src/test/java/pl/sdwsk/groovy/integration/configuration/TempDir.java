package pl.sdwsk.groovy.integration.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import pl.sdwsk.config.PicProperties;
import pl.sdwsk.utils.FileUtils;

import javax.annotation.PostConstruct;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author sdwsk
 */
@Configuration
@Order
public class TempDir {
    private static Logger log = LoggerFactory.getLogger(TempDir.class);
    private static final int ATTEMPTS = 500;

    @Autowired private PicProperties picProperties;

    @PostConstruct
    public void setUpDirectory() {
        String galleriesRoot = createTempDirectory();
        log.info("Temporary root for galleries created: {}", galleriesRoot);
        picProperties.setGalleriesRoot(galleriesRoot);
    }

    public static String createTempDirectory() {
        File baseDir = new File(System.getProperty("java.io.tmpdir"));
        String baseName = System.currentTimeMillis() + "-";

        for (int counter = 0; counter < ATTEMPTS; counter++) {
            File tempDir = new File(baseDir, baseName + counter);
            if (tempDir.mkdir()) {
                return tempDir.toString();
            }
        }
        throw new IllegalStateException(
                "Failed to create directory within "
                        + ATTEMPTS
                        + " attempts (tried "
                        + baseName
                        + "0 to "
                        + baseName
                        + (ATTEMPTS - 1)
                        + ')');

    }

    /**
     * Creates directory under provided path
     *
     * @param base target path
     * @param name directory name
     * @return directory name
     */
    public static Path createTempDirectory(Path base, String name) {
        FileUtils.createDirectory(base.resolve(name));
        return Paths.get(name);
    }

}
