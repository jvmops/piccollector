package pl.sdwsk.groovy.unit

import org.springframework.mock.web.MockMultipartFile
import pl.sdwsk.config.PicProperties
import pl.sdwsk.groovy.integration.configuration.TempDir
import pl.sdwsk.utils.UploadedFileProcessor
import spock.lang.Shared
import spock.lang.Specification

import java.nio.file.Path
import java.nio.file.Paths

/**
 * @author sdwsk
 */
class UploadedFileProcessorSpec extends Specification {
    @Shared def uploadedFileProcessor
    @Shared def picProperties
    @Shared def galleryFolderName

    def setupSpec() {
        def galleriesRoot = TempDir.createTempDirectory();
        picProperties = new PicProperties(
                applicationUrl: "http://piccollector.sdwsk.pl/galleries",
                galleriesRoot: galleriesRoot)
        galleryFolderName = TempDir.createTempDirectory(picProperties.getGalleriesRoot(), "2016-07-20_Sample-gallery")
        uploadedFileProcessor = new UploadedFileProcessor(picProperties);
    }

    def "will provide valid filename"() {
        given:
        def author = "User"
        def timestamp = System.currentTimeMillis()
        def extension = ".jpg"

        when:
        def newFileName = uploadedFileProcessor.resolveNewFilename(author, timestamp, extension)

        then:
        newFileName instanceof Path

        and:
        newFileName.toString() == author + "_" + timestamp + extension
    }

    def "file will be transfered to the destination directory"() {
        given:
        def uploadedPicture = new MockMultipartFile("samplePicture.jpg", "asd".getBytes())
        def fileName = Paths.get("User_350902340004.jpg")
        def destination = galleryFolderName.resolve(fileName)

        when:
        uploadedFileProcessor.transferPicture(uploadedPicture, destination)

        then:
        picProperties.galleriesRoot.resolve(destination).toFile().exists()
    }

    def "url to the transfered file will be properly resolved"() {
        given:
        def uploadedPicture = new MockMultipartFile("samplePicture.jpg", "asd".getBytes())
        def fileName = Paths.get("User_350902340004.jpg")
        def destination = galleryFolderName.resolve(fileName)
        def folderDate = "2016-07-20"
        def folderName = "Sample-gallery"

        when:
        def url = uploadedFileProcessor.transferPicture(uploadedPicture, destination)

        then:
        url == picProperties.applicationUrl + "/" + folderDate + "/" + folderName + "/" + fileName.toString()
    }
}
