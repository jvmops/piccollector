package pl.sdwsk;

import org.junit.Test;
import pl.sdwsk.utils.GalleryResource;

import static org.junit.Assert.assertTrue;

public class GalleryResourceTest {

	@Test
	public void willReturnValidPath() {
		String requestedUrl = "2015-05-01/Sample-gallery/user1_0001.jpg";
		GalleryResource galleryResource = new GalleryResource(requestedUrl);
		String resourcePath = galleryResource.getResourcePath();
		assertTrue(resourcePath.equalsIgnoreCase("2015-05-01_sample-gallery/user1_0001.jpg"));
	}

	@Test
	public void willReturnValidUrlFromRelativePath() {
		String path = "2015-05-01_Sample-gallery/user1_0001.jpg";
		GalleryResource galleryResource = new GalleryResource(path);
		String url = galleryResource.getResourceUrl();
		assertTrue(url.equalsIgnoreCase("/2015-05-01/sample-gallery/user1_0001.jpg"));
	}
}
