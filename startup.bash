#!/usr/bin/env bash
java -jar piccollector-web/target/piccollector.jar \
  --spring.application.name=Piccollector \
  --spring.datasource.username=postgres \
  --spring.datasource.password=pass \
  --spring.datasource.url=jdbc:postgresql://localhost:5432/test \
  --piccollector.application-url=http://localhost:7474/galleries/ \
  --piccollector.galleries-root="$MY_PROJECTS"/piccollector/galleries \
  --server.port=7474
