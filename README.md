# piccollector
1. [TLDR](#tldr)
2. [Why should you care](#introduction)
3. [Installation info](#build)

## TLDR <a name="tldr" />

This is my first pet project written in 2016. It is a Spring MVC app with a web layer generated from thymeleaf.

## Introduction <a name="introduction" />
Let's suppose you are having a good time with your friends and everyone is taking pictures.
Would you like your friends to send them to you afterwards? I bet that everyone does but
this process is difficult.

Those pictures might not be suitable for facebook. Some people might not use fb and it's hard
to download galleries from it. Others may package them into an archives to ease the download
process but it's becoming a mess. You may say cloud but it has many other disadvantages too.
**_Piccollector_** solves this problem. Host your own galleries and ensure that all those pictures
would be safely persisted and re-shared from your own hdd.

## Running <a name="build" />
Requirements:
 1. Maven 3.1+
 2. Java 8

```
mvn package
./startup.bash
```