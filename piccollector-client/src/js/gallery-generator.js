// Image layout config for masonry
$grid = $('.grid').masonry({
    itemSelector: '.picture',
    columnWidth: '.picture-sizer',
    gutter: 5
});

$grid.imagesLoaded().progress( function() {
    $grid.masonry('layout');
});

// Event listener that is detecting selected files from file explorer menu.
$(document).on('change', ':file', function() {
    var files = $(this).prop('files');
    updateSelectedFiles(files);
});

// Event listener that is detecting dropped files.
$('.modal')
    .on('dragover', function(event) {
        event.preventDefault();
        event.stopPropagation();
    })
    .on('dragleave', function(event) {
        event.preventDefault();
        event.stopPropagation();
    })
    .on('drop', function(event) {
        event.preventDefault();
        event.stopPropagation();
        var files = event.originalEvent.dataTransfer.files;
        updateSelectedFiles(files);
    });

$(':reset').click(function() {
    clearSelectedFiles();
    updateBadge(0);
});

var selectedFiles,
    $fileStatusElements,
    galleryUrl = window.location.href;

// Empties selected files array and populates it with recent files.
function updateSelectedFiles(files) {
    clearSelectedFiles();
    $(files).each(function(index, file) {
        selectedFiles.push(file);
    });
    updateBadge(selectedFiles.length);
    $.each(selectedFiles, function (index, file) {
        appendFileStatusElement(file);
    });
    $fileStatusElements = $('#status-info').find('.file-status');
}

// clears status info div and selected files array
function clearSelectedFiles() {
    $('#status-info').empty();
    selectedFiles=[];
}

// updates button badge with selected files quantity
function updateBadge(quantity) {
    $('.badge').text(quantity);
}

// Appends file info to the status list
function appendFileStatusElement(file) {
    // callback function that is removing file info from status list
    var removeFile = function removeFile() {
        var $fileStatusElement = $(this).parents().eq(2);
        var removedFileName = $fileStatusElement.attr('id');
        selectedFiles = $.grep($(selectedFiles), function(file) {
            return removedFileName != file.name;
        });
        $fileStatusElement.hide('fast');
        updateBadge(selectedFiles.length);
    }
    
    $('#status-info').append(
        $('<div/>')
            .attr("id", file.name)
            .addClass("file-status")
            .append($('<div/>')
                .addClass("file-label")
                .append(
                    $('<span/>')
                        .addClass('tag label label-default')
                        .css('width', '150px')
                        .append(
                            $('<a/>')
                                .attr('id', 'remove-file')
                                .click(removeFile)
                                .append(
                                    $('<i/>')
                                        .addClass('remove glyphicon glyphicon-remove glyphicon-white')))
                        .append($('<span/>')
                            .text(file.name))))
            .append($('<div/>')
                .addClass('progress')
                .append($('<div/>')
                    .addClass('progress-bar')
                    .attr('role', 'progressbar')
                    .attr('role', 'progressbar')
                    .attr('aria-valuenow', '0%')
                    .attr('aria-valuemin', '0%')
                    .attr('aria-valuemax', '100%')
                    .css('width', '0%'))));
}


// Upload handling
$(document).on('submit', '#uploadForm', function(event) {
    /* Perform some button action ... */
    event.preventDefault();
    uploadNextFile();
});

// In order to send files one by one I need to call this function recursively within a success callback
function uploadNextFile() {
    if( selectedFiles.length > 0 ) {
        var file = selectedFiles[0],
            $fileStatusElement = $fileStatusElements.filter(function (index) {
                return $( this ).attr('id') === file.name;
            })
        var formData = new FormData();
            formData.append('uploadedFile', file);

        $.ajax({
            xhr: function(){
                var xhr = new window.XMLHttpRequest();
                // progress bar update
                xhr.upload.addEventListener("progress", function(evt){
                    if (evt.lengthComputable) {
                        var percentComplete = Math.round(evt.loaded * 100 / evt.total);
                        $fileStatusElement.find('.progress-bar')
                            .attr('aria-valuenow', percentComplete+'%')
                            .css('width', percentComplete+'%');
                    }
                }, false);
                return xhr;
            },
            type: 'POST',
            url: galleryUrl,
            data: formData,
            context: $fileStatusElement,
            processData: false,
            contentType: false
        }).done(function (data) {
            $(this).find('.label')
                .removeClass('label-default label-danger')
                .addClass('label-success')
                .find('a')
                .prop('onclick', null)
                .off('click')
                .find('i')
                .removeClass('glyphicon-remove')
                .addClass('glyphicon-ok');;
            $(this).find('.progress')
                .fadeOut('slow');
            selectedFiles.splice(0, 1);
            updateBadge(selectedFiles.length);
            appendImg(data);
            if($(this).hasClass('file-status-error')) {
                $(this).removeClass('file-status-error');
                $(this).find('.alert-msg').remove();
            }
            $(':submit').text('Upload');
            uploadNextFile();
        }).fail(function (jqXHR, textStatus, errorThrown) {
            if (!$(this).find('.alert-msg').length) {
                $(this).addClass('file-status-error')
                    .prepend($('<div/>')
                        .addClass('alert-msg')
                        .attr('role', 'alert')
                        .text(jqXHR.responseJSON.message));

                $(this).find('.label')
                    .removeClass('label-default')
                    .addClass('label-danger');
                $('.buttons :submit').text('Resume');
            }
        });
    }

    function appendImg(data) {
        var $myPictureContainer = $('#'+data.author+'-grid');
        // if there is yet no picture from this user prepend new container and register it with masonry
        if (!$myPictureContainer.length) {
            $myPictureContainer = $('<div/>')
                .addClass('grid')
                .attr('id', data.author + '-grid')
                .append($('<div/>')
                    .addClass('picture-sizer'))
            $('#pictures').prepend(
                $('<div/>')
                    .addClass('user-content')
                    .attr('id', data.author)
                    .append($('<h3/>')
                        .text(data.author))
                    .append($myPictureContainer)
            );
            $myPictureContainer.masonry({
                itemSelector: '.picture',
                columnWidth: '.picture-sizer',
                gutter: 5
            });
        }
        // append new picture to the container
        var element = $('<div/>')
            .addClass('picture')
            .data('fileName', data.fileName)
            .append($('<img/>')
                .attr('src', data.url))
            .append($('<div/>')
                .attr('id', 'eye')
                .append($('<a/>')
                    .attr('href', '#')
                    .append($('<span/>')
                        .addClass('glyphicon glyphicon-eye-open'))))
            .append($('<div/>')
                .addClass('cover')
                .append($('<a/>')
                    .attr('href', '#')
                    .attr('id', "star")
                    .on('click', function(event) {
                        setCover(event, $(this));})
                    .append($('<span/>')
                        .addClass('glyphicon glyphicon-star-empty'))))
            .hover(slideUp, slideDown);

        $myPictureContainer.append(element)
            .masonry('appended', element)
            .masonry('layout');
    }
}

var slideUp = function () {
        $(this).find('.cover').stop(true, false).animate({ height: "30px" });
        $(this).find('#eye').stop(true, false).fadeIn(500)
    },
    slideDown = function () {
        $(this).find('.cover').stop(true, false).animate({ height: "0px" });
        $(this).find('#eye').stop(true, false).fadeOut(500)
    };

// pictures on hover effect
$('.picture').hover(slideUp, slideDown);

// album cover setting after clicking 'star'
$('a#star').on('click', function(event) {
    setCover(event, $(this));
});

function setCover(event, $starButton) {
    event.preventDefault();
    var fileName = $starButton.closest('.picture').data('fileName');
    $.ajax({
        type: 'POST',
        url: galleryUrl,
        data: 'fileName=' + fileName,
        contentType: 'application/x-www-form-urlencoded',
        context: $(this)
    }).done(function (data) {
        console.log('done')
    }).fail(function () {
        console.log('failed')
    });
}